FROM registry.access.redhat.com/ubi8/python-38

ARG CI_PROJECT_URL
#USER 0
RUN wget -O - https://gitlab.com/api/v4/projects/4207231/packages/generic/graphviz-releases/2.49.3/graphviz-2.49.3.tar.gz | tar xz && cd graphviz-2.49.3 && ./configure --prefix=/opt/app-root && make && make install && cd .. && rm -rf graphviz-2.49.3
#RUN yum --disableplugin=subscription-manager -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm && yum --disableplugin=subscription-manager -y update && yum --disableplugin=subscription-manager -y search graphviz && yum --disableplugin=subscription-manager clean all
#RUN yum --disableplugin=subscription-manager -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm && yum --disableplugin=subscription-manager -y install graphviz && yum --disableplugin=subscription-manager clean all
#RUN yum --disableplugin=subscription-manager -y install http://rpmfind.net/linux/centos/8.5.2111/AppStream/x86_64/os/Packages/graphviz-2.40.1-43.el8.x86_64.rpm
#RUN dnf install graphviz
#USER 1001
RUN wget -O - https://gitlab.com/api/v4/projects/4207231/packages/generic/graphviz-releases/2.49.3/graphviz-2.49.3.tar.gz | tar xz && cd graphviz-2.49.3 && ./configure && make
RUN python3 -m pip install wheel
RUN python3 -m pip install git+${CI_PROJECT_URL}
RUN which hflav_webserver
RUN which neato
