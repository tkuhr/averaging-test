# Naming Conventions for Parameters

The recommended format of parameter names is the following

    TYPE_MOTHER_DAUGHTERS

where `TYPE` is the type of parameter, `MOTHER` the name of the mother particle, and `DAUGHTERS` the names of daughter particles separated by underscores.

The possible parameter types are
* `BR`: branching fraction
* `ACP`: CP asymmetry
* `Azero`, `Aperp`, `Apara`: polarization amplitudes
* `Deltaperp`, `Deltapara`: relative polarization amplitude phases
* `fL`, `fperp`, `fpara`: polarization fractions
* `AA`: generic type for angular analyses (the exact name of the parameter can be added using a custom string, see the end of this page)
* `IA`: isospin asymmetry


Particles (and not anti-particles) are used for mother particles, following the PDG convention.

Daughter particles are ordered by descending mass and then within an isospin multiplet by descending (absolute) charge (`pi+` before `pi-` before `pi0`) or particle before anti-particle.

The name of anti-particles that are written with a bar in latex is given by the name of the particle with `bar` added, e.g. `pbar`, `K0bar`, `Lambdac-bar`.

Valid particle names are listed in the following table in the recommended order:
<table>
<tr><th>name</th><th>latex</th><th>PDG ID</th></tr>
<tr><td>Bc+</td> <td>B_c^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S091">S091</a></td></tr>
<tr><td>Bc-</td> <td>B_c^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S091">S091</a></td></tr>
<tr><td>Omegab-</td> <td>\Omega_b^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S063">S063</a></td></tr>
<tr><td>Omegab+bar</td> <td>\bar{\Omega}_b^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S063">S063</a></td></tr>
<tr><td>Xib-</td> <td>\Xi_b^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S060">S060</a></td></tr>
<tr><td>Xib+bar</td> <td>\bar{\Xi}_b^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S060">S060</a></td></tr>
<tr><td>Xib0</td> <td>\Xi_b^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S060">S060</a></td></tr>
<tr><td>Xib0bar</td> <td>\bar{\Xi}_b^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S060">S060</a></td></tr>
<tr><td>Lambdab0</td> <td>\Lambda_b^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S040">S040</a></td></tr>
<tr><td>Lambdab0bar</td> <td>\bar{\Lambda}_b^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S040">S040</a></td></tr>
<tr><td>Bs0</td> <td>B_s^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S086">S086</a></td></tr>
<tr><td>Bs0bar</td> <td>\bar{B}_s^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S086">S086</a></td></tr>
<tr><td>Badmix</td> <td>B</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S049">S049</a></td></tr>
<tr><td>B+</td> <td>B^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S041">S041</a></td></tr>
<tr><td>B-</td> <td>B^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S041">S041</a></td></tr>
<tr><td>B0</td> <td>B^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S042">S042</a></td></tr>
<tr><td>B0bar</td> <td>\bar{B}^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S042">S042</a></td></tr>
<tr><td>psi(4660)</td> <td>\psi(4660)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M189">M189</a></td></tr>
<tr><td>Yeta</td> <td>Y_\eta</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=???">???</a></td></tr>
<tr><td>Zc(4430)+</td> <td>Z_c(4430)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M195">M195</a></td></tr>
<tr><td>Zc(4430)-</td> <td>Z_c(4430)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M195">M195</a></td></tr>
<tr><td>Pc(4457)+</td> <td>P_c(4457)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B172">B172</a></td></tr>
<tr><td>Pc(4457)-bar</td> <td>\bar{P}_c(4457)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B172">B172</a></td></tr>
<tr><td>Pc(4380)+</td> <td>P_c(4380)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B171">B171</a></td></tr>
<tr><td>Pc(4380)-bar</td> <td>\bar{P}_c(4380)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B171">B171</a></td></tr>
<tr><td>psi(4260)</td> <td>\psi(4260)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M074">M074</a></td></tr>
<tr><td>X(4250)+</td> <td>X(4250)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M192">M192</a></td></tr>
<tr><td>X(4250)-</td> <td>X(4250)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M192">M192</a></td></tr>
<tr><td>Zc(4200)+</td> <td>Z_c(4200)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M231">M231</a></td></tr>
<tr><td>Zc(4200)-</td> <td>Z_c(4200)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M231">M231</a></td></tr>
<tr><td>chic1(4274)</td> <td>\chi_{c1}(4274)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M233">M233</a></td></tr>
<tr><td>chic1(4140)</td> <td>\chi_{c1}(4140)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M193">M193</a></td></tr>
<tr><td>X(4050)+</td> <td>X(4050)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M191">M191</a></td></tr>
<tr><td>X(4050)-</td> <td>X(4050)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M191">M191</a></td></tr>
<tr><td>Y(3940)</td> <td>Y(3940)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=???">???</a></td></tr>
<tr><td>X(3915)</td> <td>X(3915)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M159">M159</a></td></tr>
<tr><td>Zc(3900)+</td> <td>Z_c(3900)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=???">???</a></td></tr>
<tr><td>Zc(3900)-</td> <td>Z_c(3900)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=???">???</a></td></tr>
<tr><td>X(3872)+</td> <td>X(3872)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=???">???</a></td></tr>
<tr><td>X(3872)-</td> <td>X(3872)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=???">???</a></td></tr>
<tr><td>X(3872)</td> <td>X(3872)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M176">M176</a></td></tr>
<tr><td>psi2(3823)</td> <td>\psi_2(3823)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M212">M212</a></td></tr>
<tr><td>psi(3770)</td> <td>\psi(3770)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M053">M053</a></td></tr>
<tr><td>psi2S</td> <td>\psi(2S)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M071">M071</a></td></tr>
<tr><td>etac2S</td> <td>\eta_c(2S)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M059">M059</a></td></tr>
<tr><td>chic2</td> <td>\chi_{c2}</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M057">M057</a></td></tr>
<tr><td>hc</td> <td>h_c</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M144">M144</a></td></tr>
<tr><td>chic1</td> <td>\chi_{c1}</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M055">M055</a></td></tr>
<tr><td>chic0</td> <td>\chi_{c0}</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M056">M056</a></td></tr>
<tr><td>Jpsi</td> <td>J/\psi</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M070">M070</a></td></tr>
<tr><td>etac</td> <td>\eta_c</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M026">M026</a></td></tr>
<tr><td>Xic0(2930)</td> <td>\Xi_c(2930)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B156">B156</a></td></tr>
<tr><td>Xic0(2930)bar</td> <td>\bar{\Xi}_c(2930)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B156">B156</a></td></tr>
<tr><td>Sigmac(2800)++</td> <td>\Sigma_c(2800)^{++}</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B155">B155</a></td></tr>
<tr><td>Sigmac(2800)--bar</td> <td>\bar{\Sigma}_c(2800)^{--}</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B155">B155</a></td></tr>
<tr><td>Sigmac(2800)+</td> <td>\Sigma_c(2800)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B155">B155</a></td></tr>
<tr><td>Sigmac(2800)-bar</td> <td>\bar{\Sigma}_c(2800)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B155">B155</a></td></tr>
<tr><td>Sigmac(2800)0</td> <td>\Sigma_c(2800)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B155">B155</a></td></tr>
<tr><td>Sigmac(2800)0bar</td> <td>\bar{\Sigma}_c(2800)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B155">B155</a></td></tr>
<tr><td>Lambdac(2940)+</td> <td>\Lambda_c(2940)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B122">B122</a></td></tr>
<tr><td>Lambdac(2940)-bar</td> <td>\bar{\Lambda}_c(2940)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B122">B122</a></td></tr>
<tr><td>Lambdac(2880)+</td> <td>\Lambda_c(2880)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B151">B151</a></td></tr>
<tr><td>Lambdac(2880)-bar</td> <td>\bar{\Lambda}_c(2880)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B151">B151</a></td></tr>
<tr><td>Lambdac(2860)+</td> <td>\Lambda_c(2860)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B178">B178</a></td></tr>
<tr><td>Lambdac(2860)-bar</td> <td>\bar{\Lambda}_c(2860)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B178">B178</a></td></tr>
<tr><td>Lambdac(2765)+</td> <td>\Lambda_c(2765)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B150">B150</a></td></tr>
<tr><td>Lambdac(2765)-bar</td> <td>\bar{\Lambda}_c(2765)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B150">B150</a></td></tr>
<tr><td>Lambdac(2625)+</td> <td>\Lambda_c(2625)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B102">B102</a></td></tr>
<tr><td>Lambdac(2625)-bar</td> <td>\bar{\Lambda}_c(2625)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B102">B102</a></td></tr>
<tr><td>Lambdac(2595)+</td> <td>\Lambda_c(2595)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B119">B119</a></td></tr>
<tr><td>Lambdac(2595)-bar</td> <td>\bar{\Lambda}_c(2595)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B119">B119</a></td></tr>
<tr><td>Ds1*(2860)+</td> <td>D_{s1}^*(2860)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M196">M196</a></td></tr>
<tr><td>Ds1*(2860)-</td> <td>D_{s1}^*(2860)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M196">M196</a></td></tr>
<tr><td>Ds1*(2700)+</td> <td>D_{s1}^*(2700)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M182">M182</a></td></tr>
<tr><td>Ds1*(2700)-</td> <td>D_{s1}^*(2700)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M182">M182</a></td></tr>
<tr><td>Ds2*(2573)+</td> <td>D_{s2}^*(2573)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M148">M148</a></td></tr>
<tr><td>Ds2*(2573)-</td> <td>D_{s2}^*(2573)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M148">M148</a></td></tr>
<tr><td>Ds1(2536)+</td> <td>D_{s1}(2536)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M121">M121</a></td></tr>
<tr><td>Ds1(2536)-</td> <td>D_{s1}(2536)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M121">M121</a></td></tr>
<tr><td>Ds1(2460)+</td> <td>D_{s1}(2460)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M173">M173</a></td></tr>
<tr><td>Ds1(2460)-</td> <td>D_{s1}(2460)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M173">M173</a></td></tr>
<tr><td>Ds0*(2317)+</td> <td>D_{s0}^*(2317)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M172">M172</a></td></tr>
<tr><td>Ds0*(2317)-</td> <td>D_{s0}^*(2317)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M172">M172</a></td></tr>
<tr><td>Xic+</td> <td>\Xi_c^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S045">S045</a></td></tr>
<tr><td>Xic-bar</td> <td>\bar{\Xi}_c^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S045">S045</a></td></tr>
<tr><td>Xic0</td> <td>\Xi_c^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S048">S048</a></td></tr>
<tr><td>Xic0bar</td> <td>\bar{\Xi}_c^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S048">S048</a></td></tr>
<tr><td>D1(2430)0</td> <td>D_1(2430)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M180">M180</a></td></tr>
<tr><td>D1(2430)0bar</td> <td>\bar{D}_1(2430)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M180">M180</a></td></tr>
<tr><td>D2*+</td> <td>D_2^*(2460)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M150">M150</a></td></tr>
<tr><td>D2*-</td> <td>D_2^*(2460)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M150">M150</a></td></tr>
<tr><td>D1+</td> <td>D_1(2420)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M120">M120</a></td></tr>
<tr><td>D1-</td> <td>D_1(2420)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M120">M120</a></td></tr>
<tr><td>D0*+</td> <td>D_0^*(2300)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M179">M179</a></td></tr>
<tr><td>D0*-</td> <td>D_0^*(2300)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M179">M179</a></td></tr>
<tr><td>D2*0</td> <td>D_2^*(2460)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M119">M119</a></td></tr>
<tr><td>D2*0bar</td> <td>\bar{D}_2^*(2460)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M119">M119</a></td></tr>
<tr><td>D10</td> <td>D_1(2420)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M097">M097</a></td></tr>
<tr><td>D10bar</td> <td>\bar{D}_1(2420)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M097">M097</a></td></tr>
<tr><td>D0*0</td> <td>D_0^*(2300)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M178">M178</a></td></tr>
<tr><td>D0*0bar</td> <td>\bar{D}_0^*(2300)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M178">M178</a></td></tr>
<tr><td>Sigmac*++</td> <td>\Sigma_c(2520)^{++}</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B115">B115</a></td></tr>
<tr><td>Sigmac*--bar</td> <td>\bar{\Sigma}_c(2520)^{--}</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B115">B115</a></td></tr>
<tr><td>Sigmac*+</td> <td>\Sigma_c(2520)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B115">B115</a></td></tr>
<tr><td>Sigmac*-bar</td> <td>\bar{\Sigma}_c(2520)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B115">B115</a></td></tr>
<tr><td>Sigmac*0</td> <td>\Sigma_c(2520)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B115">B115</a></td></tr>
<tr><td>Sigmac*0bar</td> <td>\bar{\Sigma}_c(2520)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B115">B115</a></td></tr>
<tr><td>Sigmac++</td> <td>\Sigma_c(2455)^{++}</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B104">B104</a></td></tr>
<tr><td>Sigmac--bar</td> <td>\bar{\Sigma}_c(2455)^{--}</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B104">B104</a></td></tr>
<tr><td>Sigmac+</td> <td>\Sigma_c(2455)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B104">B104</a></td></tr>
<tr><td>Sigmac-bar</td> <td>\bar{\Sigma}_c(2455)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B104">B104</a></td></tr>
<tr><td>Sigmac0</td> <td>\Sigma_c(2455)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B104">B104</a></td></tr>
<tr><td>Sigmac0bar</td> <td>\bar{\Sigma}_c(2455)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B104">B104</a></td></tr>
<tr><td>Lambdac+</td> <td>\Lambda_c^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S033">S033</a></td></tr>
<tr><td>Lambdac-bar</td> <td>\bar{\Lambda}_c^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S033">S033</a></td></tr>
<tr><td>fJ(2220)</td> <td>f_J(2220)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M082">M082</a></td></tr>
<tr><td>Ds*+</td> <td>D_s^{*+}</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S074">S074</a></td></tr>
<tr><td>Ds*-</td> <td>D_s^{*-}</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S074">S074</a></td></tr>
<tr><td>K4*(2045)+</td> <td>K_4^*(2045)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M035">M035</a></td></tr>
<tr><td>K4*(2045)-</td> <td>K_4^*(2045)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M035">M035</a></td></tr>
<tr><td>K4*(2045)0</td> <td>K_4^*(2045)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M035">M035</a></td></tr>
<tr><td>K4*(2045)0bar</td> <td>\bar{K}_4^*(2045)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M035">M035</a></td></tr>
<tr><td>f2(2010)</td> <td>f_2(2010)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M106">M106</a></td></tr>
<tr><td>D*+</td> <td>D^*(2010)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M062">M062</a></td></tr>
<tr><td>D*-</td> <td>D^*(2010)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M062">M062</a></td></tr>
<tr><td>D*0</td> <td>D^*(2007)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M061">M061</a></td></tr>
<tr><td>D*0bar</td> <td>\bar{D}^*(2007)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M061">M061</a></td></tr>
<tr><td>Ds+</td> <td>D_s^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S034">S034</a></td></tr>
<tr><td>Ds-</td> <td>D_s^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S034">S034</a></td></tr>
<tr><td>D+</td> <td>D^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S031">S031</a></td></tr>
<tr><td>D-</td> <td>D^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S031">S031</a></td></tr>
<tr><td>D0</td> <td>D^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S032">S032</a></td></tr>
<tr><td>D0bar</td> <td>\bar{D}^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S032">S032</a></td></tr>
<tr><td>tau-</td> <td>\tau^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S035">S035</a></td></tr>
<tr><td>tau+</td> <td>\tau^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S035">S035</a></td></tr>
<tr><td>K0*(1950)0</td> <td>K_0^*(1950)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M134">M134</a></td></tr>
<tr><td>K0*(1950)0bar</td> <td>\bar{K}_0^*(1950)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M134">M134</a></td></tr>
<tr><td>K0*(1950)+</td> <td>K_0^*(1950)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M134">M134</a></td></tr>
<tr><td>K0*(1950)-</td> <td>K_0^*(1950)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M134">M134</a></td></tr>
<tr><td>K2(1820)0</td> <td>K_2(1820)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M146">M146</a></td></tr>
<tr><td>K2(1820)0bar</td> <td>\bar{K}_2(1820)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M146">M146</a></td></tr>
<tr><td>K2(1820)+</td> <td>K_2(1820)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M146">M146</a></td></tr>
<tr><td>K2(1820)-</td> <td>K_2(1820)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M146">M146</a></td></tr>
<tr><td>K3*(1780)+</td> <td>K_3^*(1780)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M060">M060</a></td></tr>
<tr><td>K3*(1780)-</td> <td>K_3^*(1780)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M060">M060</a></td></tr>
<tr><td>K3*(1780)0</td> <td>K_3^*(1780)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M060">M060</a></td></tr>
<tr><td>K3*(1780)0bar</td> <td>\bar{K}_3^*(1780)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M060">M060</a></td></tr>
<tr><td>K2(1770)+</td> <td>K_2(1770)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M023">M023</a></td></tr>
<tr><td>K2(1770)-</td> <td>K_2(1770)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M023">M023</a></td></tr>
<tr><td>K2(1770)</td> <td>K_2(1770)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M023">M023</a></td></tr>
<tr><td>K*(1680)0</td> <td>K^*(1680)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M095">M095</a></td></tr>
<tr><td>K*(1680)0bar</td> <td>\bar{K}^*(1680)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M095">M095</a></td></tr>
<tr><td>K*(1680)+</td> <td>K^*(1680)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M095">M095</a></td></tr>
<tr><td>K*(1680)-</td> <td>K^*(1680)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M095">M095</a></td></tr>
<tr><td>f0(1710)</td> <td>f_0(1710)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M068">M068</a></td></tr>
<tr><td>phi(1680)</td> <td>\phi(1680)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M067">M067</a></td></tr>
<tr><td>Omega-</td> <td>\Omega^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S024">S024</a></td></tr>
<tr><td>Omega+bar</td> <td>\bar{\Omega}^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S024">S024</a></td></tr>
<tr><td>rho(1700)+</td> <td>\rho(1700)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M065">M065</a></td></tr>
<tr><td>rho(1700)-</td> <td>\rho(1700)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M065">M065</a></td></tr>
<tr><td>rho(1700)0</td> <td>\rho(1700)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M065">M065</a></td></tr>
<tr><td>rho3(1690)+</td> <td>\rho_3(1690)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M015">M015</a></td></tr>
<tr><td>rho3(1690)-</td> <td>\rho_3(1690)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M015">M015</a></td></tr>
<tr><td>rho3(1690)0</td> <td>\rho_3(1690)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M015">M015</a></td></tr>
<tr><td>Lambda(1520)</td> <td>\Lambda(1520)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B038">B038</a></td></tr>
<tr><td>Lambda(1520)bar</td> <td>\bar{\Lambda(1520)}</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B038">B038</a></td></tr>
<tr><td>f2prime(1525)</td> <td>f_2^{\prime}(1525)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M013">M013</a></td></tr>
<tr><td>f0(1500)</td> <td>f_0(1500)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M152">M152</a></td></tr>
<tr><td>eta(1475)</td> <td>\eta(1475)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M175">M175</a></td></tr>
<tr><td>rho(1450)+</td> <td>\rho(1450)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M105">M105</a></td></tr>
<tr><td>rho(1450)-</td> <td>\rho(1450)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M105">M105</a></td></tr>
<tr><td>rho(1450)0</td> <td>\rho(1450)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M105">M105</a></td></tr>
<tr><td>a0(1450)+</td> <td>a_0(1450)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M149">M149</a></td></tr>
<tr><td>a0(1450)-</td> <td>a_0(1450)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M149">M149</a></td></tr>
<tr><td>K2*(1430)+</td> <td>K_2^*(1430)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M022">M022</a></td></tr>
<tr><td>K2*(1430)-</td> <td>K_2^*(1430)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M022">M022</a></td></tr>
<tr><td>K2*(1430)0</td> <td>K_2^*(1430)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M022">M022</a></td></tr>
<tr><td>K2*(1430)0bar</td> <td>\bar{K}_2^*(1430)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M022">M022</a></td></tr>
<tr><td>f1(1420)</td> <td>f_1(1420)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M006">M006</a></td></tr>
<tr><td>(Kpi)0*+</td> <td>(K\pi)^{*+}_{0}</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=???">???</a></td></tr>
<tr><td>(Kpi)0*-</td> <td>(K\pi)^{*-}_{0}</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=???">???</a></td></tr>
<tr><td>(Kpi)0*0</td> <td>(K\pi)^{*0}_{0}</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=???">???</a></td></tr>
<tr><td>K0*(1430)+</td> <td>K_0^*(1430)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M019">M019</a></td></tr>
<tr><td>K0*(1430)-</td> <td>K_0^*(1430)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M019">M019</a></td></tr>
<tr><td>K0*(1430)0</td> <td>K_0^*(1430)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M019">M019</a></td></tr>
<tr><td>K0*(1430)0bar</td> <td>\bar{K}_0^*(1430)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M019">M019</a></td></tr>
<tr><td>K*(1410)+</td> <td>K^*(1410)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M094">M094</a></td></tr>
<tr><td>K*(1410)-</td> <td>K^*(1410)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M094">M094</a></td></tr>
<tr><td>K*(1410)0</td> <td>K^*(1410)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M094">M094</a></td></tr>
<tr><td>K*(1410)0bar</td> <td>\bar{K}^*(1410)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M094">M094</a></td></tr>
<tr><td>eta(1405)</td> <td>\eta(1405)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M027">M027</a></td></tr>
<tr><td>K1(1400)+</td> <td>K_1(1400)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M064">M064</a></td></tr>
<tr><td>K1(1400)-</td> <td>K_1(1400)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M064">M064</a></td></tr>
<tr><td>K1(1400)0</td> <td>K_1(1400)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M064">M064</a></td></tr>
<tr><td>K1(1400)0bar</td> <td>\bar{K}_1(1400)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M064">M064</a></td></tr>
<tr><td>Sigma(1385)-</td> <td>\Sigma(1385)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B043">B043</a></td></tr>
<tr><td>Sigma(1385)+bar</td> <td>\bar{\Sigma(1385)}^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B043">B043</a></td></tr>
<tr><td>Sigma(1385)0</td> <td>\Sigma(1385)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B043">B043</a></td></tr>
<tr><td>Sigma(1385)0bar</td> <td>\bar{\Sigma(1385)}^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B043">B043</a></td></tr>
<tr><td>Sigma(1385)+</td> <td>\Sigma(1385)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B043">B043</a></td></tr>
<tr><td>Sigma(1385)-bar</td> <td>\bar{\Sigma(1385)}^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B043">B043</a></td></tr>
<tr><td>f0(1370)</td> <td>f_0(1370)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M147">M147</a></td></tr>
<tr><td>Xi-</td> <td>\Xi^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S022">S022</a></td></tr>
<tr><td>Xi+bar</td> <td>\bar{\Xi}^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S022">S022</a></td></tr>
<tr><td>a2+</td> <td>a_2(1320)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M012">M012</a></td></tr>
<tr><td>a2-</td> <td>a_2(1320)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M012">M012</a></td></tr>
<tr><td>a20</td> <td>a_2(1320)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M012">M012</a></td></tr>
<tr><td>Xi0</td> <td>\Xi^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S023">S023</a></td></tr>
<tr><td>Xi0bar</td> <td>\bar{\Xi}^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S023">S023</a></td></tr>
<tr><td>eta(1295)</td> <td>\eta(1295)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M037">M037</a></td></tr>
<tr><td>f1</td> <td>f_1(1285)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M008">M008</a></td></tr>
<tr><td>f2</td> <td>f_2(1270)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M005">M005</a></td></tr>
<tr><td>K1(1270)+</td> <td>K_1(1270)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M028">M028</a></td></tr>
<tr><td>K1(1270)-</td> <td>K_1(1270)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M028">M028</a></td></tr>
<tr><td>K1(1270)0</td> <td>K_1(1270)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M028">M028</a></td></tr>
<tr><td>K1(1270)0bar</td> <td>\bar{K}_1(1270)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M028">M028</a></td></tr>
<tr><td>b1+</td> <td>b_1(1235)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M011">M011</a></td></tr>
<tr><td>b1-</td> <td>b_1(1235)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M011">M011</a></td></tr>
<tr><td>b10</td> <td>b_1(1235)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M011">M011</a></td></tr>
<tr><td>Delta+</td> <td>\Delta(1232)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B033">B033</a></td></tr>
<tr><td>Delta-</td> <td>\Delta(1232)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B033">B033</a></td></tr>
<tr><td>Delta0</td> <td>\Delta(1232)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B033">B033</a></td></tr>
<tr><td>Delta0bar</td> <td>\bar{\Delta(1232)}^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=B033">B033</a></td></tr>
<tr><td>a1+</td> <td>a_1(1260)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M010">M010</a></td></tr>
<tr><td>a1-</td> <td>a_1(1260)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M010">M010</a></td></tr>
<tr><td>a10</td> <td>a_1(1260)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M010">M010</a></td></tr>
<tr><td>Sigma+</td> <td>\Sigma^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S019">S019</a></td></tr>
<tr><td>Sigma-bar</td> <td>\bar{\Sigma}^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S019">S019</a></td></tr>
<tr><td>Sigma-</td> <td>\Sigma^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S020">S020</a></td></tr>
<tr><td>Sigma+bar</td> <td>\bar{\Sigma}^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S020">S020</a></td></tr>
<tr><td>Sigma0</td> <td>\Sigma^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S021">S021</a></td></tr>
<tr><td>Sigma0bar</td> <td>\bar{\Sigma}^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S021">S021</a></td></tr>
<tr><td>Lambda0</td> <td>\Lambda^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S018">S018</a></td></tr>
<tr><td>Lambda0bar</td> <td>\bar{\Lambda}^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S018">S018</a></td></tr>
<tr><td>phi</td> <td>\phi(1020)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M004">M004</a></td></tr>
<tr><td>f0</td> <td>f_0(980)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M003">M003</a></td></tr>
<tr><td>a0+</td> <td>a_0(980)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M036">M036</a></td></tr>
<tr><td>a0-</td> <td>a_0(980)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M036">M036</a></td></tr>
<tr><td>a00</td> <td>a_0(980)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M036">M036</a></td></tr>
<tr><td>etaprime</td> <td>\eta^\prime</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M002">M002</a></td></tr>
<tr><td>p</td> <td>p</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S016">S016</a></td></tr>
<tr><td>pbar</td> <td>\bar{p}</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S016">S016</a></td></tr>
<tr><td>n</td> <td>n</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S017">S017</a></td></tr>
<tr><td>nbar</td> <td>\bar{n}</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S017">S017</a></td></tr>
<tr><td>K*+</td> <td>K^*(892)^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M018">M018</a></td></tr>
<tr><td>K*-</td> <td>K^*(892)^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M018">M018</a></td></tr>
<tr><td>K*0</td> <td>K^*(892)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M018">M018</a></td></tr>
<tr><td>K*0bar</td> <td>\bar{K}^*(892)^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M018">M018</a></td></tr>
<tr><td>omega</td> <td>\omega(782)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M001">M001</a></td></tr>
<tr><td>rho+</td> <td>\rho^+(770)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M009">M009</a></td></tr>
<tr><td>rho-</td> <td>\rho^-(770)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M009">M009</a></td></tr>
<tr><td>rho0</td> <td>\rho^0(770)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M009">M009</a></td></tr>
<tr><td>eta</td> <td>\eta</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S014">S014</a></td></tr>
<tr><td>K+</td> <td>K^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S010">S010</a></td></tr>
<tr><td>K-</td> <td>K^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S010">S010</a></td></tr>
<tr><td>K0</td> <td>K^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S011">S011</a></td></tr>
<tr><td>K0bar</td> <td>\bar{K}^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S011">S011</a></td></tr>
<tr><td>K0S</td> <td>K^0_S</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S012">S012</a></td></tr>
<tr><td>K0L</td> <td>K^0_L</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S013">S013</a></td></tr>
<tr><td>f0(500)</td> <td>f_0(500)</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=M014">M014</a></td></tr>
<tr><td>pi+</td> <td>\pi^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S008">S008</a></td></tr>
<tr><td>pi-</td> <td>\pi^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S008">S008</a></td></tr>
<tr><td>pi0</td> <td>\pi^0</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S009">S009</a></td></tr>
<tr><td>s</td> <td>s</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=Q123">Q123</a></td></tr>
<tr><td>mu-</td> <td>\mu^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S004">S004</a></td></tr>
<tr><td>mu+</td> <td>\mu^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S004">S004</a></td></tr>
<tr><td>e-</td> <td>e^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S003">S003</a></td></tr>
<tr><td>e+</td> <td>e^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S003">S003</a></td></tr>
<tr><td>l-</td> <td>\ell^-</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=??">??</a></td></tr>
<tr><td>l+</td> <td>\ell^+</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=??">??</a></td></tr>
<tr><td>nu</td> <td>\nu</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S066">S066</a></td></tr>
<tr><td>nubar</td> <td>\bar{\nu}</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S066">S066</a></td></tr>
<tr><td>nul</td> <td>\nu_{\ell}</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S066">S066</a></td></tr>
<tr><td>nulbar</td> <td>\bar{\nu}_{\ell}</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S066">S066</a></td></tr>
<tr><td>nutau</td> <td>\nu_{\tau}</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S066">S066</a></td></tr>
<tr><td>nutaubar</td> <td>\bar{\nu}_{\tau}</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S066">S066</a></td></tr>
<tr><td>numu</td> <td>\nu_{\mu}</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S066">S066</a></td></tr>
<tr><td>numubar</td> <td>\bar{\nu}_{\mu}</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S066">S066</a></td></tr>
<tr><td>nue</td> <td>\nu_{e}</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S066">S066</a></td></tr>
<tr><td>nuebar</td> <td>\bar{\nu}_{e}</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S066">S066</a></td></tr>
<tr><td>gamma</td> <td>\gamma</td> <td><a href="https://pdglive.lbl.gov/Particle.action?node=S000">S000</a></td></tr>
<tr><td>K*</td> <td>K^*</td> <td></td></tr>
<tr><td>K</td> <td>K</td> <td></td></tr>
<tr><td>pi</td> <td>\pi</td> <td></td></tr>
<tr><td>cc</td> <td>\mathrm{+c.c.}</td> <td></td></tr>
<tr><td>NR</td> <td>\mathrm{(NR)}</td> <td></td></tr>
<tr><td>plus</td> <td> + </td> <td></td></tr>
</table>


A dot is used for the products of two parameters

    PARAMETER_A.PARAMETER_B

and two dots for the ratio of two parameters

    PARAMETER_A..PARAMETER_B

Arbitrary, custom strings can be added to a parameter name by enclosing them with double underscores, e.g. `__CustomString__`.
If the custom string is at the end of the parameter name only prepending double underscores are required.
No latex code can be generated from parameter names with custom strings.
