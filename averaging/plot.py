#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from datetime import date
from copy import deepcopy
import os
from .error import toError


subtitle = None

def set_subtitle(title):
    global subtitle
    subtitle = title


experiment_colors = {
    'ATLAS': 'black',
    'BaBar': 'green',
    'Belle': 'blue',
    'CDF': 'red',
    'CMS': 'orange',
    'D0': 'magenta',
    'LHCb': 'cyan',
    }

dpi = 120


def hflav_logo(fig, scale=1):
    """Add the HFLAV logo to the figure"""

    global subtitle
    if subtitle is None:
        subtitle = str(date.today())

    ypixel = 25
    xyratio = 4.8
    ysub = 0.9
    fontsize = 12
    fontsub = 0.75
    offset = -0.05
    xsize, ysize = fig.transFigure.inverted().transform((scale * xyratio * ypixel, scale * (1 + ysub) * ypixel))
    fraction = ysub / (1 + ysub)
    font = {'family': 'sans-serif', 'style': 'italic', 'color': 'white', 'weight': 'bold', 'size': scale * fontsize}

    save_axes = plt.gca()
    ax = plt.axes((0, 1-ysize, xsize, ysize), label='logo', frame_on=False)
    ax.set_axis_off()
    plt.fill([0, 1, 1, 0], [fraction, fraction, 1, 1], 'k', edgecolor='k', linewidth=0.5)
    plt.text(0.5, 0.5 * (1 + fraction) + offset, 'HFLAV', fontdict=font, ha='center', va='center')
    plt.fill([0, 1, 1, 0], [0, 0, fraction, fraction], 'w', edgecolor='k', linewidth=0.5)
    font['color'] = 'black'
    font['size'] *= fontsub
    plt.text(0.5, 0.5 * fraction + offset, subtitle, fontdict=font, ha='center', va='center')
    plt.sca(save_axes)


def min_max_limit(entries, log=False):
    """Get range covered by values (entry[0]) with statistical (entry[1]) and systematic (entry[2]) errors and determine if they contain a limit. If log is True negative values are ignored."""

    min = None
    max = None
    limit = False
    for entry in entries:
        if entry[0] is None:
            continue
        lower = upper = entry[0]
        if entry[1] is None:
            limit = True
        else:
            error = deepcopy(entry[1])
            if entry[2] is not None:
                error.add(entry[2])
            lower += error.neg
            upper += error.pos
        if (min is None or min > lower) and (not log or lower > 0):
            min = lower
        if max is None or max < upper:
            max = upper

    return (min, max, limit)


def plot_measurement(y, value, stat_error, syst_error, xmin, color = 'black', linestyle = 'solid'):
    """Add a measurement entry to a plot."""

    if value is None:
        return
    if stat_error is None:
        offset = 0 if xmin is None else xmin
        plot = plt.errorbar([value], [y], xerr=[[value - offset], [0]], xuplims=True, capsize=6, mew=2, color=color, linestyle=linestyle)
        plot[1][0].set_marker(matplotlib.markers.CARETLEFT)
        plot[1][0].set_markeredgewidth(0)
    else:
        error = deepcopy(stat_error)
        if syst_error is not None:
            error.add(syst_error)
        plot = plt.errorbar([value], [y], xerr=[[abs(error.neg)], [error.pos]], fmt='o', capsize=0, color=color, linestyle=linestyle)
        plt.errorbar([value], [y], xerr=[[abs(stat_error.neg)], [stat_error.pos]], capsize=6, mew=2, color=color)

    return plot


def result_plot(parameter, measurements_dict, filename, options=None):
    """Create a plot of average and individual measurements."""

    # select measurements with information about the parameter of interest and check if they contain a limit
    measurements = []
    entries = []
    for measurement_hash, input in parameter.average_inputs.items():
        if 'value' in input.keys() and input['value'] is not None:
            measurement = measurements_dict[measurement_hash]
            measurements.append(measurement)
            entries.append([input['value'], toError(input['stat_error']), toError(input['syst_error'])])
    xmin, xmax, limit = min_max_limit(entries)
    xmin = 0 if (limit and xmin > 0) else None

    # create the plot axes
    n = len(measurements)
    dy = 1. / (n + 1)
    fig = plt.figure(figsize=(8, (n+3) * 0.4), dpi=dpi, constrained_layout=True)
    plt.title(f'${parameter.latex}$')
    plt.axis(ymin=0, ymax=1)
    plt.yticks([])

    # plot the average
    if parameter.average_error is None:
        plt.bar(0, 1, parameter.average_value, 0, align='edge', color='yellow')
    else:
        plt.axvspan(parameter.average_value + parameter.average_error.neg, parameter.average_value + parameter.average_error.pos, color='yellow')
        plt.axvline(parameter.average_value, color='black')

    # plot the PDG average
    if parameter.pdg_value is not None and (options is None or 'nopdg' not in options):
        if parameter.pdg_error is None:
            plt.bar(0, 1, parameter.pdg_value, 0, align='edge', color='lightsteelblue', alpha=0.5)
        else:
            plt.axvspan(parameter.pdg_value + parameter.pdg_error.neg, parameter.pdg_value + parameter.pdg_error.pos, color='lightsteelblue', alpha=0.5)
            plt.axvline(parameter.pdg_value, color='grey', linestyle='--')

    # plot each measurement
    y = n * dy
    legend_entries = []
    legend_labels = []
    for measurement in measurements:
        input = parameter.average_inputs[measurement.hash()]
        color = experiment_colors.get(measurement.publication.experiment, 'black')
        plot = plot_measurement(y, input['value'], toError(input['stat_error']), toError(input['syst_error']), xmin, color)
        if measurement.publication.experiment not in legend_labels:
            legend_labels.insert(0, measurement.publication.experiment)
            legend_entries.insert(0, plot)
        y -= dy

    if xmin is not None:
        plt.xlim(left=xmin)

    # draw legend, logo and save figure
    if parameter.pdg_value is not None and (options is None or 'nopdg' not in options):
        legend_labels.insert(0, 'PDG')
        legend_entries.insert(0, (matplotlib.patches.Patch(color='lightsteelblue', alpha=0.5), matplotlib.lines.Line2D([], [], color='grey', marker=[(0,1), (0,-0.3)], linestyle='None')))
    legend_labels.insert(0, 'HFLAV')
    legend_entries.insert(0, (matplotlib.patches.Patch(color='yellow'), matplotlib.lines.Line2D([], [], color='black', marker='|', linestyle='None')))
    try:
        plt.legend(legend_entries, legend_labels)
    except:
        print('WARNING: Plotting of legend for %s failed.' % self.name)
    hflav_logo(fig)
    if not os.path.exists(os.path.dirname(filename)):
        os.makedirs(os.path.dirname(filename))
    try:
        plt.savefig(filename, format='png', dpi=dpi)
    except Exception as exception:
        print('WARNING: The saving of %s failed:' % filename)
        print(exception)
    plt.close()


def overview_plot(parameters, filename, title = None, options=None):
    """Create an overview plot."""

    # create the plot axes
    n = len(parameters)
    width = 10
    height = (n+3) * 0.4
    fig = plt.figure(figsize=(width, height), dpi=dpi, constrained_layout=True)
    ax = plt.axes()
    if title is not None:
        plt.title(title)
    plt.axis(ymin=0, ymax=n)
    plt.grid(True)

    # determine minimal axis value
    entries = [[parameter.average_value, parameter.average_error, None] for parameter in parameters]
    log = options is not None and 'log' in options
    xmin, xmax, limit = min_max_limit(entries, log)
    if log:
        plt.xscale('log')
        xmin *= (xmin / xmax)**0.05
    elif limit and xmin > 0:
        xmin = 0
    else:
        xmin = None

    # plot each parameter
    y = 0.5
    yticks = []
    averages = []
    for parameter in reversed(parameters):
        plot = plot_measurement(y, parameter.average_value, parameter.average_error, None, xmin)
        averages.append(parameter.average_value) # there is probably a better way to do that
        yticks.append(r'$' + parameter.latex + '$')
        y += 1

    plt.yticks(0.5+np.arange(n), yticks)
    if xmin is not None:
        plt.xlim(left=xmin)

    # add logo and save figure
    hflav_logo(fig)
    plt.savefig(filename, format='png', dpi=dpi)

    # create click map
    map = f'<map name="map_{os.path.splitext(os.path.basename(filename))[0]}">\n'
    y = 0.5
    for parameter in reversed(parameters):
        xrange = fig.transFigure.transform([(1, 1)])
        scale = width*dpi / xrange[0][0]
        yrange = scale * ax.transData.transform([(0, y-0.4), (0, y+0.4)])
        map += f'  <area shape="rect" coords="{0},{height*dpi-yrange[1][1]},{width*dpi},{height*dpi-yrange[0][1]}" href="{parameter.name}.html">\n'
        y += 1
    map += '</map>\n'

    plt.close()
    return map
