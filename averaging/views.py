from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.conf import settings
from .models import ParameterData, plot_info, PageView
import json
import uuid
import pathlib
import os
from .particles import particle_filter_names, particle_categories
from .latex import latex_result
from .plot import overview_plot, set_subtitle
from .parameter import Parameter
from .error import toError


def home(request):
    return HttpResponseRedirect("/index")


def health(request):
    """Takes an request as a parameter and gives the count of pageview objects as reponse. Function made for Liveness probe
    used when deploying using openshift."""
    return HttpResponse(PageView.objects.count())


def particle2html(filter):
    """Replace characters that cannot be used as a html ID"""
    return filter.replace('*', 'S').replace('+', 'plus')


def index(request):
    """This function servers as controller for the "index" view in the website."""

    # based on particle_categories determine which filter should appear in which category
    initial_context = [[filter, particle_filter_names[filter]] for filter in particle_categories['Initial particle']]
    observable_context = [[filter, particle_filter_names[filter]] for filter in particle_categories['Type of observable']]
    daughter_context = []
    for category in particle_categories:
        if category not in ['Initial particle', 'Type of observable']:
            daughter_context.append([category, [[particle2html(filter), particle_filter_names[filter]] for filter in particle_categories[category]]])
    daughter_num_list = [[particle2html(filter), particle_filter_names[filter]] for filter in particle_filter_names.keys()]
    context = {'title': settings.TITLE, 'initial': initial_context, 'obs': observable_context, 'daughter': daughter_context, 'num': daughter_num_list}
    return render(request, "averaging/index.html", context)


def build_config(data):
    """Takes the request POST data and builds a dictionary containing various filter names and their required quantity as specified by the website user."""

    config = {}
    if 'initial' in data.keys():
        config[str(data['initial'][0])] = 1
    if 'observable' in data.keys():
        config[str(data['observable'][0])] = 1
    for filter in particle_filter_names.keys():
        key = particle2html(filter)
        if data[key] != None and int(data[key][0]) != 0:
            config[str(filter)] = int(data[key][0])
    return config


def satisfy_criteria(config, filter):
    """Check if selected parameter meets user's demands as per the config."""

    for item in config:
        if item not in filter.keys():
            return False
        else:
            if int(config[item]) > int(filter[item]):
                return False
    return True


def post_form(request):
    """This function is activated everytime user makes a ajax request."""

    if request.is_ajax and request.method == 'POST':
        if request.POST:
            config = build_config(dict(request.POST))
            result_json = []
            for parameter in ParameterData.objects.all():
                if satisfy_criteria(config, parameter.data['filters']):
                    result_json.append({'latex': parameter.data['latex'], 'value': parameter.data['result'], 'id': parameter.data['name'], 'order': parameter.data['value']})
            del config
            result_json.sort(key=lambda item: item['order'], reverse=True)

            return JsonResponse(json.dumps(result_json), safe=False)
        else:
            return JsonResponse(json.dumps({"error": "some form error"}), status=400)


def view_detail(request, id):
    """This function serves as controller for details page of every parameter."""

    parameter = ParameterData.objects.filter(data__name=id).first()
    return render(request, "averaging/detail.html", {'parameter': parameter.data})


def generate_plot(request):
    """Function takes inputs from user for which branching fractions to draw an overview plot (using an ajax request)."""

    if request.is_ajax and request.method == 'POST':
        selected = str(request.POST.get('selected')).rstrip(',').split(',')
        parameters = []
        for name in selected:
            data = ParameterData.objects.filter(data__name=name).first().data
            parameter = Parameter({'name': data['name'], 'latex': data['latex'][1:-1]})
            parameter.average_value = data['value']
            if 'error' in data.keys():
                parameter.average_error = toError(data['error'])
            parameters.append(parameter)
        parameters.sort(key=lambda item: item.average_value, reverse=True)

        fileid = uuid.uuid1()
        filename = f'/static/user_gen_plots/{fileid}.png'
        filepath = str(pathlib.Path().resolve()) + filename
        if not os.path.exists(os.path.dirname(filepath)):
            os.mkdir(os.path.dirname(filepath))
        img_obj = plot_info(img_id=fileid, img_path=filepath)
        img_obj.save()
        img_obj.chk_expiry()
        set_subtitle(settings.PLOT_SUBTITLE)
        overview_plot(parameters, filepath, title='Overview Plot of Selected Observables')

        return JsonResponse(json.dumps({'filepath': filename, 'id': str(fileid)}), safe=False)
