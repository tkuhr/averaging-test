from .averaging import Averaging
from .latex import LatexOutput
from .html import HtmlOutput
from .dump import DumpOutput
from .website import WebSite
from .plot import set_subtitle
import os
import json
import argparse
import sys
from datetime import date


parser = argparse.ArgumentParser(description='HFLAV averaging tool')
group = parser.add_mutually_exclusive_group()
parser.add_argument('--symmetrize', type=float, default=0.05, help='symmetrize uncertainties if the positive and negative uncertainties differ from the Hesse uncertainty by less than the given fraction')
parser.add_argument('-v', '--verbose', dest='verbose', action='store_true', help='print averaging results')
parser.add_argument('-w', '--warn-all', dest='warn_all', action='store_true', help='enable all warnings')
parser.add_argument('--warn-unused', action='store_true', help='warn if there is a parameter without any measurements')
parser.add_argument('--warn-undefined', action='store_true', help='warn if there is an undefined nuisance parameter')
parser.add_argument('--warn-unknown', action='store_true', help='warn if an unknown parameter is used in an output')
parser.add_argument('--warn-output', action='store_true', help='warn if a parameter is not included or appears multiple times in the (latex or html) output')
parser.add_argument('--warn-names', action='store_true', help='warn if a parameter name contains forbidden characters or does not follow the naming convention or is defined multiple times')
parser.add_argument('--warn-p-value', type=float, help='warn if the fit p-value is below the given value')
parser.add_argument('--warn-pdg-pub', action='store_true', help='warn if a PDG average uses a publication that is not considered here')
parser.add_argument('--warn-pdg-value', type=float, help='warn if a PDG average deviates from the calculated average by more than the given number of standard deviations')
parser.add_argument('--warn-pdg-error', type=float, help='warn if the uncertainty of a PDG average deviates from the uncertainty of the calculated average by more than the given factor')
parser.add_argument('--warn-pdg-value-if-same', type=float, help='warn if a PDG average deviates from the calculated average by more than the given number of standard deviations in case the same measurements are used')
parser.add_argument('--warn-pdg-error-if-same', type=float, help='warn if the uncertainty of a PDG average deviates from the uncertainty of the calculated average by more than the given factor in case the same measurements are used')
parser.add_argument('-f', '--filter', dest='filter', help='a regular expression to select a subset of parameters for averaging; be careful to escape dot (\\.), star (\\*), and plus (\\+) if used as part of a parameter name')
parser.add_argument('--nuisances', action='store_true', help='print overview of nuisance parameter usage')
parser.add_argument('--fit-statistics', help='name of csv file to store fit information')
parser.add_argument('-l', '--latex', dest='latex', help='name of a json file containing the structure of latex output, default is no latex output')
parser.add_argument('--latex-dir', default='latex', help='for latex output: name of output directory')
parser.add_argument('--latex-global-footnotes', action='store_true', default=False, help='for latex output: do not repeat footnotes if they appeared in previous tables')
parser.add_argument('--latex-pdg-always', action='store_true', default=False, help='for latex output: always show PDG averages even if they are identical to the determined ones')
parser.add_argument('--latex-show-indirect', action='store_true', default=False, help='for latex output: if a measurement is used in the fit, but does not directly give the parameter of interest, show what quantity is measured in the table instead of a footnote')
parser.add_argument('--html', dest='html', help='name of a json file containing the structure of html output, default is no html output')
parser.add_argument('--html-dir', default='html', help='for html output: name of output directory')
parser.add_argument('--html-debug', action='store_true', help='for html output: include debug information')
parser.add_argument('--plot-subtitle', default=str(date.today()), help='for plots: subtitle in HFLAV logo')
parser.add_argument('--dump', help='name of a directory for json files containing all information required for displaying averages, default is no dump output')
parser.add_argument('--web', action='store_true', help='create a django web site')
parser.add_argument('--web-title', default='HFLAV Averages', help='title of web site')
parser.add_argument('-o', '--output', dest='output', help='name of a json file in which the fitted parameter values will be stored')
group.add_argument('-i', '--input', dest='input', help='name of a json file from which the fitted parameter values are read')
args = parser.parse_args()

if args.plot_subtitle is not None:
    set_subtitle(args.plot_subtitle)

averaging = Averaging(args.__dict__)

def load_structure(filename):
    try:
        return json.loads(open(filename).read())
    except Exception as e:
        print(f'ERROR: failed to load and parse {args.latex} -> {str(e)}')
        sys.exit(1)

if args.latex:
    structure = load_structure(args.latex)
    latex = LatexOutput(args.__dict__)
    latex.run(averaging, structure)

if args.html:
    structure = load_structure(args.html)
    html = HtmlOutput(args.__dict__)
    html.run(averaging, structure)

if args.dump:
    dump = DumpOutput(args.dump)
    dump.run(averaging)

if args.web:
    web = WebSite(averaging)
