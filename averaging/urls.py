from django.urls import path
from django.conf.urls import url
from . import views


urlpatterns = [
    path('', views.home, name="home"),
    path('index/post/ajax/filter', views.post_form , name='post_form'),
    path('index/post/ajax/plot', views.generate_plot, name='generate_plot'),
    url(r'detail/(?P<id>[-\w. + ( ) *]+)/$', views.view_detail, name='view_detail'),
    path('index', views.index, name='index'),
    url(r'^health$', views.health),
]
