#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import math
import subprocess
from .plot import result_plot
from .latex import latex_result, latex_average
from .error import toError
from .particles import particles, particle_indices


class DisplayData:
    """Class to store the averaging results data in a format suitable for display on web pages"""

    def __init__(self, averaging, create_plots=True):
        """Create display data and plots from Averaging object."""

        self.averaging = averaging
        plot_dir = 'static'
        if create_plots and not os.path.exists(plot_dir):
            try:
                os.mkdir(plot_dir)
            except OSError:
                print(f'ERROR: could not create directory {plot_dir}')

        self.parameters = {}

        for name, parameter in averaging.parameters.items():
            if parameter.type != 'fit' or parameter.average_value is None:
                continue
            entry = self.create_parameter_data(parameter)

            plot_file = f'{plot_dir}/{name}.png'
            if create_plots and not os.path.isfile(plot_file):
                result_plot(parameter, self.averaging.measurements, plot_file)
            if os.path.isfile(plot_file):
                entry['plot'] = os.path.basename(plot_file)

            plot_file = f'{plot_dir}/{name}_dependencies.png'
            if create_plots and not os.path.isfile(plot_file):
                dot_file = plot_file.replace('.png', '.dot')
                dependent_parameters = [name] + list(parameter.average_correlations.keys())
                if len(dependent_parameters) > 1:
                    self.averaging.dependency_graph(dependent_parameters, filename=dot_file, highlights=[name])
                    try:
                        result = subprocess.run(['neato', '-Tpng', '-o', plot_file, dot_file])
                    except:
                        pass
            if os.path.isfile(plot_file):
                entry['dependency_plot'] = os.path.basename(plot_file)

            self.parameters[name] = entry


    def create_comments(self, comments):
        """Return a string with the given comments."""

        if comments is None:
            return ''

        if not isinstance(comments, list):
            comments = [comments]

        result = ''
        for comment in comments:
            if comment in self.averaging.comments.keys():
                comment = self.averaging.comments[comment]
            if comment is False:
                continue
            if comment[-1] != '.':
                comment += '.'
            result += comment + ' '

        return result


    def create_parameter_data(self, parameter):
        """Create dictionary for the display of a parameter."""

        # overview list
        unit_exp, digits = parameter.get_unit_digits()
        unit_str = f' \\times 10^{{{unit_exp}}}' if unit_exp !=0 else ''
        unit = 10 ** unit_exp
        result = {
            'name': parameter.name,
            'latex': f'${parameter.latex}$',
            'value': parameter.average_value,
            'result': f'${latex_result(unit, digits, parameter.average_value, parameter.average_error)}{unit_str}$',
            'unit_str': f' [$10^{{{unit_exp}}}$]' if unit_exp !=0 else '',
        }
        if parameter.average_error is not None:
            result['error'] = parameter.average_error.toList()

        # selection filters
        filters = {}
        for factor in parameter.name.split('.'):
            elements = factor.split('_')
            type = elements.pop(0)
            if type == 'AA':
                type = 'BR'
            elif type in ['Azero', 'Aperp', 'Apara', 'Deltaperp', 'Deltapara', 'fL', 'fperp', 'fpara', 'P']:
                type = 'polarization'
            elif type in ['fc', 'fLambdab0', 'fOmegab-', 'fXib0Lambdab0', 'fXib-Lambdab0']:
                type = 'production'
            elif type not in ['BR', 'ACP']:
                type = 'other'
            filters[type] = 1
            for element in elements:
                if element in particle_indices.keys():
                    particle = particles[particle_indices[element]]
                    if len(particle) > 3:
                        filter = particle[3]
                        if filter not in filters.keys():
                            filters[filter] = 0
                        filters[filter] += 1
        result['filters'] = filters

        # table of average, PDG, and inputs
        measurements = []
        features = set()

        # average
        p_digits = -math.floor(math.log10(abs(parameter.average_p)))
        entry = {
            'experiment': 'Average',
            'measurement': f'${latex_result(unit, digits, parameter.average_value, parameter.average_error)}$',
            'chi2': f'{parameter.average_chi2:.2f}',
            'reference': f'{parameter.average_p:.{p_digits + 1}f}' if p_digits < 4 else f'{(10 ** p_digits) * parameter.average_p:.2f} \\times 10^{{{-p_digits}}}',
            'comments': self.create_comments(parameter.comment),
            'debug': f'<a href="../{parameter.filename}">{parameter.name}</a>',
            'style': '',
        }
        measurements.append(entry)

        # PDG
        if parameter.pdg_value is not None:
            entry = {
                'experiment': 'PDG',
                'measurement': f'${latex_result(unit, digits, parameter.pdg_value, parameter.pdg_error)}$',
                'chi2': '',
                'reference': f'<a href="{parameter.pdg_link()}">pdgLive</a>',
                'comments': '',
                'debug': f'<a href="../pdg/{parameter.pdg_id}.json">{parameter.pdg_id}</a>',
                'style': '',
            }
            if parameter.pdg_diff():
                entry['debug'] += f', &nbsp; shift in $\\sigma$: {parameter.pdg_diff():.2f}'
            if parameter.pdg_error_diff():
                entry['debug'] += f', &nbsp; error scale factor: {parameter.pdg_error_diff():.2f}'
            diff = parameter.pdg_pubs.difference(parameter.pubs)
            if len(diff) > 0:
                entry['debug'] += '<br>Publications included in PDG, but not this average: ' + ', '.join([f'<a href="https://inspirehep.net/literature/{id}">{id}</a>' for id in diff])
            measurements.append(entry)

        # measurements
        for key, input in parameter.average_inputs.items():
            if key in self.averaging.parameters.keys() or 'value' not in input.keys():
                continue
            measurement = self.averaging.measurements[key]
            entry = {
                'experiment': measurement.publication.experiment,
                'chi2': f'{float(input["chi2"]):.2f}' if input['chi2'] >= 0 else '',
                'reference': measurement.publication.link(),
                'comments': self.create_comments(measurement.comment),
                'debug': f'<a href="../publications/{measurement.publication.experiment}/{measurement.publication.inspire}.json">{measurement.publication.inspire}</a>',
                'style': '',
            }
            if input['value'] is not None:
                entry['measurement'] = f'${latex_result(unit, unit_exp - math.floor(math.log10(input["precision"])), input["value"], toError(input.get("stat_error", None)), toError(input.get("syst_error", None)))}$'
                if len(input.get('corrections', [])) > 0:
                    entry['measurement'] += ' using ' + ', '.join([f'${self.averaging.parameters[name].latex}$' for name in input['corrections']])
                if input.get('stat_error', None) is None:
                    features.add('limit')
            else:
                entry['measurement'] = f'${input["latex"]}$'
            color = measurement.get_color(parameter)
            if color is not None:
                entry['style'] = f' style="color:{color}"'
                features.add(color)
            measurements.append(entry)
        result['measurements'] = measurements

        # table features
        result['features'] = ''
        if len(features) > 0:
            if 'red' in features:
                result['features'] += 'Input values that appear in red are not included in the PDG average.<br>'
            if 'blue' in features:
                result['features'] += 'Input values in blue correspond to yet unpublished results.<br>'
            if 'limit' in features:
                result['features'] += 'Quoted upper limits are at 90% confidence level (CL), unless mentioned otherwise.<br>'

        # correlations
        entries = []
        for name, correlation in sorted(parameter.average_correlations.items()):
            par = self.averaging.parameters[name]
            if par.type == 'fit':
                entries.append([par, correlation, None])
        for key, input in parameter.average_inputs.items():
            if key in self.averaging.parameters.keys():
                entries.append([self.averaging.parameters[key], parameter.average_correlations[key], float(input['chi2'])])
        entries.sort(key=lambda entry: abs(entry[1]), reverse=True)
        order = {'fit': 0, 'external': 1, 'nuisance': 2}
        entries.sort(key=lambda entry: order[entry[0].type])

        correlations = []
        for entry in entries:
            par = entry[0]
            correlations.append({
                'name': par.name,
                'source': par.type,
                'parameter': par.name if par.latex is None else f'${par.latex}$',
                'correlation': f'{100 * entry[1]:.1f}',
                'value': f'${latex_average(par)}$',
                'chi2': f'{entry[2]:.2f}' if entry[2] is not None else '',
                'debug': f'<a href="../{par.filename}">{par.name}</a>'
            })
            features.add(par.type)
        result['correlations'] = correlations

        # correlations feature explanation
        result['correlation_features'] = ''
        if 'fit' in features:
            result['correlation_features'] += 'Parameters of interest whose average is determined from individual measurements are called <i>fit</i> parameters.<br>'
        if 'external' in features:
            result['correlation_features'] += 'Parameters that are needed by the fit (in particular daughter branching fraction) and whose average is not determined here, but taken from somewhere else (usually PDG) are called <i>external</i> parameters.<br>'
        if 'nuisance' in features:
            result['correlation_features'] += 'Auxiliary fit parameters without direct physical meaning (in particular those to model correlated systematic uncertainties) are called <i>nuisance</i> parameters and constrained by a normally distributed likelihood function.<br>'

        return result
