#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import json
import math
import subprocess
from .error import toError
from .particles import particles, particle_indices


class DumpOutput:
    """Class to generate json files containing all information to display averages"""

    def __init__(self, dirname):
        """Create directory for dumps."""

        self.dirname = dirname
        if not os.path.exists(dirname):
            os.makedirs(dirname)


    def add_error(self, dict, error, attribute='error'):
        """Add error attribute to a dictionary."""

        if error is None:
            return
        if error.symmetric():
            dict[attribute] = error.pos
        else:
            dict[f'{attribute}_pos'] = error.pos
            dict[f'{attribute}_neg'] = error.neg


    def add_comments(self, dict, comments, attribute='comments'):
        """Add comments attribute to a dictionary."""

        if comments is None:
            return
        if not isinstance(comments, list):
            comments = [comments]
        result = []
        for comment in comments:
            if comment in self.averaging.comments.keys():
                comment = self.averaging.comments[comment]
            result.append(comment)
        dict[attribute] = result

        return result


    def write_parameter(self, parameter):
        """Write all information about a parameter to a json file."""

        result = {
            'id': parameter.name,
            'latex': parameter.latex,
            'chi2': parameter.average_chi2,
            'ndf': parameter.average_ndf,
            'p': parameter.average_p,
            'value': parameter.average_value,
            'filter': {}
        }
        result['unit_exp'], result['digits'] = parameter.get_unit_digits()
        self.add_error(result, parameter.average_error)
        self.add_comments(result, parameter.comment)

        # filter
        name = parameter.name
        if '.' in name:
            name = name.split('.', 1)[0]
        elements = name.split('_')
        type = elements.pop(0)
        if type in ['Azero', 'Aperp', 'Apara', 'Deltaperp', 'Deltapara', 'fL', 'fperp', 'fpara']:
            type = 'polarization'
        result['filter'][type] = 1
        for element in elements:
            if element in particle_indices.keys():
                particle = particles[particle_indices[element]]
                if len(particle) > 3:
                    filter = particle[3]
                    if filter not in result['filter'].keys():
                        result['filter'][filter] = 0
                    result['filter'][filter] += 1

        # PDG
        if parameter.pdg_value is not None:
            result['pdg_value'] = parameter.pdg_value
            self.add_error(result, parameter.pdg_error, 'pdg_error')
            result['pdg_link'] = parameter.pdg_link()

        # measurements
        measurements = []
        for key, input in parameter.average_inputs.items():
            if key in self.averaging.parameters.keys():
                continue
            measurement = self.averaging.measurements[key]
            link, text = measurement.publication.link_text() 
            entry = {
                'experiment': measurement.publication.experiment,
                'link': link,
                'text': text,
            }
            if measurement.get_color(parameter) is not None:
                entry['color'] = measurement.get_color(parameter)
            if input['value'] is not None:
                entry['value'] = input['value']
                self.add_error(entry, toError(input.get('stat_error', None)), 'stat_error')
                self.add_error(entry, toError(input.get('syst_error', None)), 'syst_error')
                entry['digits'] = result['unit_exp'] - math.floor(math.log10(input["precision"]))
                if len(input.get('corrections', [])) > 0:
                    entry['using'] = ', '.join([f'${self.averaging.parameters[name].latex}$' for name in input['corrections']])
            else:
                entry['latex'] = input['latex']
            if input['chi2'] >= 0:
                entry['chi2'] = input['chi2']
            self.add_comments(entry, measurement.comment)
            measurements.append(entry)
        result['measurements'] = measurements

        # correlations
        entries = []
        for name, correlation in sorted(parameter.average_correlations.items()):
            par = self.averaging.parameters[name]
            if par.type == 'fit':
                entries.append([par, correlation, None])
        for key, input in parameter.average_inputs.items():
            if key in self.averaging.parameters.keys():
                entries.append([self.averaging.parameters[key], parameter.average_correlations[key], float(input["chi2"])])
        entries.sort(key=lambda entry: abs(entry[1]), reverse=True)
        order = {'fit': 0, 'external': 1, 'nuisance': 2}
        entries.sort(key=lambda entry: order[entry[0].type])

        correlations = []
        for entry in entries:
            par = entry[0]
            cor = {
                'type': par.type,
                'name': par.name,
                'value': par.average_value,
                'correlation': entry[1]
            }
            if par.latex is not None:
                cor['latex'] = par.latex 
            self.add_error(cor, par.average_error)
            cor['unit_exp'], cor['digits'] = par.get_unit_digits()
            if entry[2] is not None:
                cor['chi2'] = entry[2]
            correlations.append(cor)
        result['correlations'] = correlations

        with open(os.path.join(self.dirname, parameter.name + '.json'), 'w') as json_file:
            json_file.write(json.dumps(result, indent=4))

        # dependency graph
            dependent_parameters = [parameter.name] + list(parameter.average_correlations.keys())
            basefilename = os.path.join(self.dirname, parameter.name)
            self.averaging.dependency_graph(dependent_parameters, filename=basefilename + '.dot', highlights=[parameter.name])
            try:
                result = subprocess.run(['neato', '-Tpng', '-o', basefilename + '_dependencies.png', basefilename + '.dot'])
            except:
                pass


    def run(self, averaging):
        """Produce json files for all averages."""

        self.averaging = averaging
        for name in self.averaging.filter(averaging.parameters.keys()):
            parameter = self.averaging.parameters[name]
            if parameter.average_value is not None and parameter.type == 'fit':
                self.write_parameter(parameter)
