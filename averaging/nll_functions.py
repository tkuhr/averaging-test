#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def nll_sym_gauss(x, mean, sigma):
    """Negative log likelihood of symmetric Gaussian function"""

    d = (x - mean) / sigma
    return 0.5 * d * d


def nll_asym_gauss(x, mean, sigma_pos, sigma_neg):
    """Negative log likelihood of asymmetric Gaussian function with variable width linear in variance, see arXiv:physics/0406120.
    For deviations of more than sigma_+(-) the variance is fixed to sigma_+(-)^2."""

    d = x - mean
    V = sigma_pos * sigma_neg + (sigma_pos - sigma_neg) * d
    Vmin = min(sigma_pos * sigma_pos, sigma_neg * sigma_neg)
    if V < Vmin:
        V = Vmin
    Vmax = max(sigma_pos * sigma_pos, sigma_neg * sigma_neg)
    if V > Vmax:
        V = Vmax
    return 0.5 * d * d / V
