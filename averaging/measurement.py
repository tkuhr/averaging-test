#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from copy import deepcopy
from hashlib import sha256
from .parser import parse_formula, parse_expression
from .error import Error


class Measurement:
    """Representation of a measurement."""

    def __init__(self, dct):
        """Initialize the measurement object from a dictionary."""

        self.comment = None      # a literal comment or an identifier of a comment or an array of those that will be displayed as footnote(s)
        self.superseded = False  # a flag that indicates if the result is superseded
        self.color = None        # a color value used for the display of the measurement, a value of False will disable any color setting
        self.result = None       # a string representation of the measured quantity and its central value and uncertainties
        self.inputs = None       # array of string representations of values and uncertainties used as external input
        self.nll = None          # string with formula of negative log likelihood function

        for attr in self.__dict__:
            if attr in dct.keys():
                self.__setattr__(attr, dct[attr])

        self.publication = None  # publication quoting the measurement
        self.limit = False       # a flag that indicated if this is a limit
        self.value = None        # central value or limit obtained from self.result
        self.precision = None    # precision of central value obtained from self.result
        self.stat_error = None   # statistical error obtained from self.result
        self.syst_error = None   # systematic error obtained from self.result
        self.syst_sum = False    # a flag that indicates if multiple systematic errors are combined
        self.parameters = None   # set of parameters to which the measurement is sensitive
        self.nuisances = None    # set of nuisance parameters to which the measurement is sensitive


    def hash(self):
        """Calculate hash from publication reference and result or nll string"""

        text = self.publication.link() + (self.result if self.result is not None else self.nll)
        return sha256(text.encode('utf-8')).hexdigest()


    def create_nll(self, parameters, used_parameters, warn_location=None):
        """Create the negative log likelihood function either from the nll string or the result and inputs"""

        if self.nll is None:
            self.result_dict = parse_expression(self.result, parameters, used_parameters, warn_location)
            self.parameters = self.result_dict['parameters']
            self.nuisances = set()
            if 'correlations' in self.result_dict.keys():
                self.nuisances = set(self.result_dict['correlations'].keys())

            if 'limit' in self.result_dict.keys():
                self.value = self.result_dict['limit']
                self.precision = self.result_dict['precision']
                self.limit = True
                for name in list(self.parameters):
                    parameters[name].limits.append(self)
                cl = self.result_dict['cl']
                if cl != 90:
                    if self.comment is None:
                        self.comment = []
                    elif not isinstance(self.comment, list):
                        self.comment = [self.comment]
                    self.comment.append(f'CL={cl}\\,\\%.')
                return

            x = '(' + self.result_dict['observable'] + ')'

            self.value = self.result_dict['value']
            self.precision = self.result_dict['precision']
            self.stat_error = deepcopy(self.result_dict['stat_error'])
            error = deepcopy(self.stat_error)
            if 'syst_error' in self.result_dict and self.result_dict['syst_error'] is not None:
                self.syst_error = deepcopy(self.result_dict['syst_error'])
                error.add(self.syst_error)
                self.syst_sum = self.result_dict.get('syst_sum', False)
                if self.syst_sum:
                    if self.comment is None:
                        self.comment = []
                    elif not isinstance(self.comment, list):
                        self.comment = [self.comment]
                    self.comment.append('syst_sum')

            if 'correlations' in self.result_dict:
                for name, scale in self.result_dict['correlations'].items():
                    x = f'({x} + {scale}*par[{parameters[name].index}])'
                    if self.syst_error is None:
                        self.syst_error = Error(scale)
                    else:
                        self.syst_error.add(Error(scale))

            if self.inputs is not None:
                for input in self.inputs:
                    input_dict = parse_expression(input, parameters, used_parameters, warn_location)
                    result_error = input_dict['stat_error'] * self.value / input_dict['value']
                    if not input_dict.get('additional', False):
                        error.subtract(result_error)
                    else:
                        self.syst_error.add(result_error)
                    if input_dict['scaling'] > 0:
                        x += f' * ({input_dict["value"]}/{input_dict["observable"]})'
                    else:
                        x += f' * ({input_dict["observable"]}/{input_dict["value"]})'
                    self.nuisances = self.nuisances.union(input_dict['parameters'])

            if error.symmetric():
                self.nll = f'nll_sym_gauss({x}, {self.value}, {error.pos})'
            else:
                self.nll = f'nll_asym_gauss({x}, {self.value}, {error.pos}, {abs(error.neg)})'

            # sums to determine simple weighted mean of measurements
            if len(self.parameters) == 1:
                parameter = parameters[list(self.parameters)[0]]
                w = error.weight()
                parameter.sumwx += w * self.result_dict['value']
                parameter.sumw += w

            for name in list(self.parameters) + list(self.nuisances):
                parameters[name].measurements.append(self)
                parameters[name].pubs.add(self.publication.inspire)

        else:
            self.nll, self.parameters = parse_formula(self.nll, parameters, used_parameters)


    def get_color(self, parameter):
        """Color for preliminary results or results not included in the PDG average or an explicitly set color"""

        if self.color is None:
            return self.publication.color(parameter)
        elif self.color is False:
            return None
        return self.color
