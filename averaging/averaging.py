#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from .parameter import Parameter
from .measurement import Measurement
from .publication import Publication
from .error import Error, errorToList, toError
from .nll_functions import nll_sym_gauss, nll_asym_gauss
from .latex import latex_result
from iminuit import Minuit
from scipy import stats
from copy import deepcopy
import json
import os
import math
import time
import requests
import re
import pkg_resources
import random


global json_filename  # name of json file that is currently read
json_filename = None


def decode_json(dct):
    """Create objects from dictionaries read from json"""

    if 'name' in dct:
        return Parameter(dct, json_filename)
    elif 'measurements' in dct:
        return Publication(dct)
    elif 'result' in dct or 'nll' in dct:
        return Measurement(dct)
    else:
        return dct



class Averaging:
    """Management of averaging process"""

    def __init__(self, config = None):
        """Initialize and run the averaging."""

        random.seed()
        if config is None:
            config = {}
        self.config = {}
        for key, item in config.items():
            if item is not None:
                self.config[key] = config[key]
        if self.config.get('warn_all', False):
            self.config['warn_unused'] = True
            self.config['warn_undefined'] = True
            self.config['warn_unknown'] = True
            self.config['warn_output'] = True
            self.config['warn_names'] = True
            self.config_default('warn_p_value', 0.01)
            self.config['warn_pdg_pub'] = True
            self.config_default('warn_pdg_value', 0.5)
            self.config_default('warn_pdg_error', 2)
            self.config_default('warn_pdg_value_if_same', 0.05)
            self.config_default('warn_pdg_error_if_same', 1.1)

        self.parameters = {}       # dictionary of names to parameter objects
        self.publications = []     # list of publication objects
        self.measurements = {}     # dictionary of measurement hashes to measurements
        self.comments = {}         # dictionary of identifiers to footnote comments
        self.nll = None            # negative log likelihood function
        self.fit_parameters = None # list of fit parameter names
        self.minuit = None         # minimizer

        self.read_parameters()
        self.get_pdg_info()
        self.read_externals()
        self.read_nuisances()
        self.read_publications()
        self.read_comments()
        self.create_nll()
        if 'input' in self.config.keys():
            self.read_result()
        else:
            self.fit_averages()
            self.set_limits()
            self.determine_average_inputs()
            self.write_result()
        if self.config.get('nuisances', False):
            self.print_nuisances()


    def config_default(self, key, value):
        """Set a default config value if none is given."""

        if key not in self.config:
           self.config[key] = value


    def filter(self, names):
        """Return a list of parameter names matching the configured filter."""

        if not self.config.get('filter', False):
            return names
        return [name for name in names if re.match(self.config['filter'], name.split(':')[0])]


    def read_json(self, filename, url = None, delay = 1):
        """Read a json file and return the parsed content. If the file does not exits download it from the given url first and wait the given number of seconds to avoid overloading the server."""

        if not os.path.exists(filename):
            if url is not None:
                if self.config.get('verbose', False):
                    print(f'Downloading {filename} from {url}')
                request = requests.get(url)
                time.sleep(delay)
                if request.status_code == requests.codes.ok:
                    with open(filename, 'wb') as downloaded_file:
                        downloaded_file.write(request.content)
                else:
                    print(f'ERROR: Failed to download {url}')
                    return None

        with open(filename) as json_file:
            global json_filename
            json_filename = filename
            try:
                return json.loads(json_file.read(), object_hook=decode_json)
            except json.decoder.JSONDecodeError as e:
                print(f'JSON error in {filename}: {str(e)}')
                return None


    def read_parameters(self):
        """Read parameter objects from parameters.json and any json file in or below the parameters directory."""

        parameter_list = []

        if os.path.exists('parameters.json'):
            parameter_list += self.read_json('parameters.json')

        for (dirname, dirs, files) in os.walk('parameters'):
            for filename in files:
                if filename.endswith('.json'):
                    parameter_list += self.read_json(os.path.join(dirname, filename))

        for parameter in parameter_list:
            if parameter.name in self.parameters.keys() and self.config.get('warn_names', False):
                print(f'WARNING: Parameter {parameter.name} defined multiple times')
            self.parameters[parameter.name] = parameter
            if self.config.get('warn_names', False):
                parameter.check_name()

        print(f'{len(parameter_list)} parameters read.')


    def read_externals(self):
        """Read parameter objects from externals.json."""

        if not os.path.exists('externals.json'):
            return

        parameter_list = self.read_json('externals.json')
        for parameter in parameter_list:
            if parameter.value is None and parameter.nll is None:
                print(f'WARNING: No value or nll given for external parameter {parameter.name}')
            parameter.type = 'external'
            self.parameters[parameter.name] = parameter
            if self.config.get('warn_names', False):
                parameter.check_name(False)

        print(f'{len(parameter_list)} external parameters read.')


    def read_nuisances(self):
        """Read parameter objects from nuisances.json and set their pdf to a normal distribution."""

        if not os.path.exists('nuisances.json'):
            return

        parameter_list = self.read_json('nuisances.json')
        for parameter in parameter_list:
            parameter.type = 'nuisance'
            parameter.nll = f'nll_sym_gauss( {parameter.name}, 0, 1)'
            self.parameters[parameter.name] = parameter
            if self.config.get('warn_names', False):
                parameter.check_name(False)

        print(f'{len(parameter_list)} nuisance parameters read.')


    def get_pdg_info(self):
        """Get information about parameter averages and considered publications from PDG."""

        pdg_ids = {}  # dictionary of PDG IDs to parameters
        for name, parameter in self.parameters.items():
            if parameter.pdg_id is not None:
                pdg_ids[parameter.pdg_id] = parameter

        if not os.path.exists('pdg'):
            try:
                os.mkdir('pdg')
            except OSError:
                print('ERROR: could not create pdg directory')
                return

        references = self.read_json('pdg/PDGIdentifiers-references.json', 'http://pdg.lbl.gov/current/PDGIdentifiers-references.json')
        for reference in references:
            for pdg_id in reference['pdgIdList']:
                if pdg_id in pdg_ids.keys():
                    pdg_ids[pdg_id].pdg_pubs.add(reference['inspireId'])

        for pdg_id, parameter in pdg_ids.items():
            if 'Desig' in pdg_id:
                desig = pdg_id.split('=')[1]
                mode = pdg_id.split(':')[0]
                pdg_info = self.read_json(f'pdg/{pdg_id}.json', f'https://pdglive.lbl.gov/BranchingRatioJson.action?desig={desig}&parCode={mode}')
            elif os.path.exists(f'pdg/{pdg_id}.json'):
                pdg_info = self.read_json(f'pdg/{pdg_id}.json')
            else:
                continue
            if pdg_info is None or 'summary' not in pdg_info.keys():
                continue
            parameter.pdg_number = pdg_info['mode_number']
            summary = pdg_info['summary'][0]
            parameter.pdg_value = float(summary['value'])
            parameter.pdg_error = Error(float(summary['error_positive']), -float(summary['error_negative']))
            if parameter.pdg_error.pos == 0 and parameter.pdg_error.neg == 0:
                parameter.pdg_error = None

        print(f'{len(pdg_ids.keys())} PDG averages read.')


    def read_publications(self):
        """Read publications and containing measurements from json files in or below the publications directory.
        Each json file must contain exactly one publication."""

        for (dirname, dirs, files) in os.walk('publications'):
            for filename in files:
                if filename.endswith('.json'):
                    self.publications.append(self.read_json(os.path.join(dirname, filename)))

        print(f'{len(self.publications)} publications with {sum([len(publication.measurements) for publication in self.publications])} measurements read.')


    def read_comments(self):
        """Read comments that will be displayed as footnotes from comments.json."""

        if os.path.exists('comments.json'):
            self.comments = self.read_json('comments.json')
            print(f'{len(self.comments.keys())} comments read.')

        if 'syst_sum' not in self.comments.keys():
            self.comments['syst_sum'] = 'Multiple systematic uncertainties are added in quadrature.'


    def create_nll(self):
        """Construct the negative log likelihood function from all measurements."""

        self.fit_parameters = []
        self.nll = 'global nll_function\ndef nll_function(par): return 0'

        for publication in self.publications:
            if publication.superseded:
                continue
            for measurement in publication.measurements:
                if measurement.superseded:
                    continue
                measurement.create_nll(self.parameters, self.fit_parameters, measurement.publication.inspire if self.config.get('warn_undefined', False) else None)
                self.measurements[measurement.hash()] = measurement
                if not measurement.limit:
                    self.nll += ' + ' + measurement.nll

        for name, parameter in self.parameters.items():
            parameter.create_nll(self.parameters, self.fit_parameters, self.config.get('warn_unused', False))
            if parameter.nll is not None:
                self.nll += ' + ' + parameter.nll

        exec(self.nll)

        print(f'Likelihood function with {len(self.fit_parameters)} parameters defined.')

        if self.config.get('warn_pdg_pub', False):
            for name, parameter in self.parameters.items():
                diff = parameter.pdg_pubs.difference(parameter.pubs)
                if len(diff) > 0:
                    print(f'WARNING: The following publications are used for the PDG average of {name}, but are not included here: {", ".join([str(id) for id in diff])}')


    def dependency_graph(self, parameters, filename=None, highlights=None):
        """Create a dot file with the dependencies among the given parameters"""

        if filename is None:
            filename = sorted(self.filter(parameters))[0] + '.dot'
        with open(filename, 'w') as dot_file:
            dot_file.write(f'strict graph {{\ngraph[overlap=false];\n" " [shape=plaintext, color=white, image="{pkg_resources.resource_filename("averaging", "legend.png")}"];\n')
            for node in parameters:
                color_map = {'fit': 'blue', 'external': 'green', 'nuisance': 'red'}
                attributes = ''
                if (highlights and node in highlights) or (highlights is None and self.config.get('filter', False) and re.match(self.config['filter'], node)):
                    attributes = ', style="filled", fillcolor=yellow'
                dot_file.write(f'"{node}" [color={color_map[self.parameters[node].type]}{attributes}];\n')
            edges = {}
            for node1 in parameters:
                for measurement in self.parameters[node1].measurements:
                    for node2 in measurement.parameters.union(measurement.nuisances):
                        if node1 == node2:
                            continue
                        if (node1, node2) not in edges.keys():
                            edges[(node1, node2)] = set()
                        edges[(node1, node2)].add(str(measurement.publication.inspire))
            for edge, pubs in edges.items():
                dot_file.write(f'"{edge[0]}" -- "{edge[1]}"  [label="{",".join(pubs)}"];\n')
            dot_file.write('}\n')


    def fit_averages(self):
        """Minimize the negative log likelihood function to determine the averages."""

        # list of fit parameters and errors
        par = [0] * len(self.fit_parameters)
        error = [0] * len(self.fit_parameters)

        # select parameters with measurements for the fit and use weighted means as starting values
        to_be_fitted = set()
        for name in self.fit_parameters:
            parameter = self.parameters[name]
            index = parameter.index
            if parameter.nll is None:
                if len(parameter.measurements) > 0:
                    to_be_fitted.add(name)
                elif len(parameter.limits) == 0 and self.config.get('warn_unused', False):
                    print(f'WARNING: Parameter {name} has no measurements.')
            if parameter.sumw != 0:
                par[index] = parameter.sumwx / parameter.sumw
                error[index] = 1 / math.sqrt(parameter.sumw)

        # check for undefined starting values and try to set them from measurements of products or ratios
        for name in self.fit_parameters:
            parameter = self.parameters[name]
            index = parameter.index
            if (par[index] == 0):
                for measurement in parameter.measurements:
                    if len(measurement.parameters) == 2 and measurement.result.startswith(name + ' ') and measurement.result.split()[1] in ['*', '/']:  # parameter of interest multiplied or divided by another parameter
                        other = self.parameters[(measurement.parameters - set([name])).pop()]
                        other_index = self.parameters[other.name].index
                        other_value = par[other_index]
                        if other_value != 0:
                            factor = other_value if measurement.result.split()[1] == '/' else (1 / other_value)
                        par[index] = measurement.value * factor
                        error[index] = measurement.stat_error.pos * factor
#                        if index == 451:
#                            print(f'*** {par[index]} {measurement.value} {factor} {measurement.result}')
                        break
            if par[index] == 0 and re.search(f' / par\[{index}\]', self.nll):
                par[index] = 0.001
                print(f'WARNING: Undefined starting value for parameter {parameter.name} used as denominator. Set to {par[index]}')

        # Minimizer
        self.minuit = Minuit(nll_function, par)
        for index in range(len(self.fit_parameters)):
            if error[index] > 0:
                self.minuit.errors[index] = error[index]
        self.minuit.errordef = Minuit.LIKELIHOOD

#        print(self.nll)
#        for name, parameter in self.parameters.items():
#            print(f'{name} {parameter.index} {parameter.sumwx} {parameter.sumw} {0 if parameter.index < 0 else par[parameter.index]}')
#        for publication in self.publications:
#            if publication.superseded:
#                continue
#            for measurement in publication.measurements:
#                if measurement.limit or measurement.superseded:
#                    continue
#                exec(f'global nll_measurement\ndef nll_measurement(par):\n    return {measurement.nll}\n')
#                chi2 = nll_measurement(self.minuit.values)
#                print(f'{chi2} {publication.inspire} {measurement.result} {measurement.nll}')


        # csv file for fit statistics
        fit_csv_name = self.config.get('fit_statistics', False)
        if fit_csv_name:
            fit_csv = open(fit_csv_name, 'w')
            fit_csv.write('parameters, fit, external, nuisance, measurements\n')

        # do fits as long as there are remaining parameters to be fitted
        while len(to_be_fitted) > 0:

            # determine a set of dependent parameters
            dependent_parameters = set()
            further_dependent_parameters = {to_be_fitted.pop()}
            while len(further_dependent_parameters) > 0:
                dependent_parameters.update(further_dependent_parameters)
                measurements = [measurement for name in further_dependent_parameters for measurement in self.parameters[name].measurements]
                further_dependent_parameters = set([name for measurement in measurements for name in measurement.parameters.union(measurement.nuisances)])
                further_dependent_parameters.difference_update(dependent_parameters)
            to_be_fitted.difference_update(dependent_parameters)

            if not self.filter(dependent_parameters):
                continue

            # fix all other parameters
            for index in range(len(self.fit_parameters)):
                self.minuit.fixed[index] = False if self.fit_parameters[index] in dependent_parameters else True

            # do the fit
#            for name in dependent_parameters:
#                print(f'{name}: {self.minuit.values[self.parameters[name].index]}')
#            print(self.minuit.fval)
            print(f'Fitting {", ".join(dependent_parameters)}')
            #print(self.minuit.values)
#            self.minuit.scan()
#            print(self.minuit.fmin)
#            self.minuit.simplex()
#            print(self.minuit.fmin)
            self.minuit.migrad()
#            for name in dependent_parameters:
#                print(f'{name}: {self.minuit.values[self.parameters[name].index]}')
#            print(self.minuit.fval)
            if not self.minuit.valid:
                print('ERROR: minimization failed')
                print(self.minuit.fmin)
                raise Exception('averaging failed')
            self.minuit.minos()

            # determine chi2 contributions and p-value
            measurements = set([measurement for name in dependent_parameters for measurement in self.parameters[name].measurements])
            chi2 = 0
            ndf = len(measurements) - len(dependent_parameters)
            for measurement in measurements:
                exec(f'global nll_measurement\ndef nll_measurement(par):\n    return {measurement.nll}\n')
                chi2_contribution = 2 * nll_measurement(self.minuit.values)
                chi2 += chi2_contribution
                for name in dependent_parameters:
                    self.parameters[name].average_inputs[measurement.hash()] = {'chi2': chi2_contribution}
            for nuisance_name in dependent_parameters:
                parameter = self.parameters[nuisance_name]
                if parameter.nll is not None:
                    exec(f'global nll_parameter\ndef nll_parameter(par):\n    return {parameter.nll}\n')
                    chi2_contribution = 2 * nll_parameter(self.minuit.values)
                    chi2 += chi2_contribution
                    ndf += 1
                    for name in dependent_parameters:
                        self.parameters[name].average_inputs[nuisance_name] = {'chi2': chi2_contribution}
            p = stats.chi2.sf(chi2, ndf) if ndf > 0 else 1
            if p < self.config.get('warn_p_value', 0):
                print(f'WARNING: p-value = {p}')

            # store the fit result in the parameter objects
            correlation = self.minuit.covariance.correlation()
            for name in dependent_parameters:
                parameter = self.parameters[name]
                index = parameter.index
                parameter.average_value = self.minuit.values[index]
                error = self.minuit.errors[index]
                merror = self.minuit.merrors[self.minuit.params[index].name]
                threshold = self.config.get('symmetrize')  # use asymmetric errors if they differ from the average one by more than the configured value
                if merror.lower_valid and merror.upper_valid and (abs(1 + merror.lower/error) > threshold or abs(1 - merror.upper/error) > threshold):
                    parameter.average_error = Error(merror.upper, merror.lower)
                else:
                    parameter.average_error = Error(error)
                for j in range(len(self.fit_parameters)):
                    if j != index and correlation[j, index] != 0:
                        parameter.average_correlations[self.fit_parameters[j]] = correlation[j, index]
                parameter.average_chi2 = chi2
                parameter.average_ndf = ndf
                parameter.average_p = p

                # compare with PDG average
                if parameter.pdg_error is not None:
                    warn_value, warn_error = (self.config.get('warn_pdg_value', math.inf), self.config.get('warn_pdg_error', math.inf))
                    if parameter.pubs == parameter.pdg_pubs:
                        warn_value, warn_error = (self.config.get('warn_pdg_value_if_same', math.inf), self.config.get('warn_pdg_error_if_same', math.inf))
                    diff = parameter.pdg_diff()
                    if abs(diff) > warn_value:
                        print(f'WARNING: Average deviates from PDG one by {diff:.2f} standard deviations.')
                    diff = parameter.pdg_error_diff()
                    if diff > warn_error or diff * warn_error < 1:
                        print(f'WARNING: Error of average deviates from PDG one by a factor {diff:.2f}.')

            # print result
            if self.config.get('verbose', False):
                print(self.minuit.fmin)
                if ndf > 0:
                    print(f'p-value = {p}')
                for index in range(len(self.fit_parameters)):
                    parameter = self.parameters[self.fit_parameters[index]]
                    if parameter.name in dependent_parameters:
                        print(parameter.name)
                        for measurement in parameter.measurements:
                            result = measurement.result.strip()
                            if result.startswith(parameter.name) and result[len(parameter.name):].strip()[0] in ['=', '<']:
                                result = result[len(parameter.name):].strip()
                            print(f' {result} ({measurement.publication.experiment})')
                        if parameter.pdg_value is not None:
                            if parameter.pdg_error is not None:
                                print(f' PDG: {parameter.pdg_value} +{parameter.pdg_error.pos} {parameter.pdg_error.neg}')
                            else:
                                print(f' PDG: < {parameter.pdg_value}')
                        print(self.minuit.params[index])
                        correlation = self.minuit.covariance.correlation()
                        for j in range(len(self.fit_parameters)):
                            if j != index and correlation[j, index] != 0:
                                print(f'{100 * correlation[j, index]}% correlation with {self.fit_parameters[j]}')
                print('')

            # csv file for fit statistics
            if fit_csv_name:
                types = [self.parameters[name].type for name in dependent_parameters]
                fit_csv.write(f'{len(dependent_parameters)}, {types.count("fit")}, {types.count("external")}, {types.count("nuisance")}, {len(measurements)}\n')


    def set_limits(self):
        """If there are only limits for a parameter take the most stringent one."""

        for name, parameter in self.parameters.items():
            if not self.filter([name]):
                continue
            if len(parameter.measurements) == 0 and len(parameter.limits) > 0:
                for limit in parameter.limits:
                    if len(limit.parameters) == 1:
                        value = limit.value
                    elif len(limit.parameters) == 2 and limit.result.startswith(name + ' ') and limit.result.split()[1] in ['*', '/']:  # parameter of interest multiplied or divided by another parameter
                        other = self.parameters[(limit.parameters - set([name])).pop()]
                        other_value = other.average_value
                        if limit.result.split()[1] == '/':
                            factor = other_value
                        else:
                            if other_value == 0:  # normalization is zero
                                continue
                            factor = 1 / other_value
                        value = limit.value * factor
                    else:
                        continue
                    if parameter.average_value is None or parameter.average_value > value:
                        parameter.average_value = value
                if self.config.get('verbose', False):
                    print(f'{name} < {parameter.average_value}')


    def determine_average_inputs(self):
        """Determine contributions of measurements to averages"""

        for name in self.filter(self.parameters.keys()):
            parameter = self.parameters[name]
            inputs = {}
            for measurement in parameter.measurements + parameter.limits:
                if measurement.superseded or measurement.publication.superseded:
                    continue
                entry = {'chi2': parameter.average_inputs[measurement.hash()]['chi2'] if measurement.hash() in parameter.average_inputs.keys() else -1}
                value = measurement.value
                precision = measurement.precision
                stat_error = deepcopy(measurement.stat_error)
                syst_error = deepcopy(measurement.syst_error)
                if len(measurement.parameters) == 1 and measurement.result.split()[1] in ['=', '<']:  # only the parameter of interest
                    pass
                elif len(measurement.parameters) == 2 and measurement.result.startswith(name + ' ') and measurement.result.split()[1] in ['*', '/']:  # parameter of interest multiplied or divided by another parameter
                    other = self.parameters[(measurement.parameters - set([name])).pop()]
                    other_value = other.average_value
                    if measurement.result.split()[1] == '/':
                        factor = other_value
                        ratio_factor = value
                    else:
                        if other_value == 0:  # normalization is zero
                            continue
                        factor = 1 / other_value
                        ratio_factor = value * factor * factor
                    value *= factor
                    precision *= abs(factor)
                    if stat_error:
                        stat_error.scale(factor)
                    if syst_error:
                        syst_error.scale(factor)
                    if other.average_error and not measurement.limit:
                        other_error = deepcopy(other.average_error)
                        other_error.scale(ratio_factor)
                        if syst_error:
                            syst_error.add(other_error)
                        else:
                            syst_error = deepcopy(other_error)
                else:  # some more complicated formular with the parameter of interest, no corrected value and errors for now
                    value = stat_error = syst_error = None
                    latex = measurement.result_dict['observable'].replace(' * ', ' ')
                    for parname in measurement.parameters:
                        par = self.parameters[parname]
                        latex = latex.replace(f'par[{par.index}]', par.latex if par.latex else par.name)
                    entry['latex'] = latex
                entry.update({'value': value, 'precision': precision, 'stat_error': errorToList(stat_error), 'syst_error': errorToList(syst_error), 'corrections': list(measurement.parameters - set([name]))})
                if value:
                    if stat_error:
                        error = deepcopy(stat_error)
                        if syst_error:
                            error.add(stat_error)
                        entry['sort1'] = error.weight()
                    else:
                        entry['sort1'] = -value
                    entry['sort2'] = ''
                else:
                    entry['sort1'] = -math.inf
                    entry['sort2'] = measurement.publication.experiment + measurement.publication.bibtex_id
                inputs[measurement.hash()] = entry

            inputs = dict(sorted(inputs.items(), key=lambda item: item[1]['sort1'], reverse=True))
            inputs = dict(sorted(inputs.items(), key=lambda item: item[1]['sort2']))
            for entry in inputs.values():
                del entry['sort1']
                del entry['sort2']
            for key, entry in sorted(parameter.average_inputs.items()):
                if key in self.parameters.keys() and self.parameters[key].type == 'external':
                    inputs[key] = entry
            for key, entry in sorted(parameter.average_inputs.items()):
                if key in self.parameters.keys() and self.parameters[key].type == 'nuisance':
                    inputs[key] = entry
            parameter.average_inputs = inputs


    def write_result(self):
        """Write fit results to a json file."""

        if 'output' in self.config.keys():
            parameters = set(self.filter(self.parameters.keys()))
            for name in list(parameters):
                for dependent in self.parameters[name].average_correlations.keys():
                    parameters.add(dependent)
            result = {}
            for name in parameters:
                result[name] = self.parameters[name].get_result_dict()
            with open(self.config['output'], 'w') as json_file:
                json_file.write(json.dumps(result, indent=4))


    def read_result(self):
        """Read fit results from a json file."""

        result = self.read_json(self.config['input'])
        for name, parameter in self.parameters.items():
            if name in result.keys():
                parameter.set_result_dict(result[name])


    def print_nuisances(self):
        """Show for each nuisance parameter in which measurements it is used."""

        print('\nNuisance parameters:')
        for name, parameter in self.parameters.items():
            if parameter.type != 'nuisance':
                continue
            print(f' {name}: {len(parameter.measurements)}')
            for measurement in parameter.measurements:
                print(f'  {measurement.publication.inspire} -> {measurement.result}')


def averaging(config = None):
    """Run the averaging machinery and return an Averaging object."""

    averaging = Averaging(config)
    return averaging
