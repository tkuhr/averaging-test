#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import math
import re
from .parser import allowed_characters, parse_number, parse_formula
from .error import Error, toError, errorToList
from .particles import particles, particle_indices

class Parameter:
    """Representation of a parameter."""

    def __init__(self, dct, filename = None):
        """Initialize the parameter object from a dictionary."""

        self.name = None       # identifier
        self.text = None       # text representation
        self.latex = None      # latex representation
        self.pdg_id = None     # identifier used by PDG
        self.comment = None    # a literal comment or an identifier of a comment or an array of those that will be displayed as footnote(s)

        self.value = None      # string representation of the central value and uncertainties in case of of an external parameter
        self.nll = None        # string with formula of negative log likelihood function in case of an external parameter or a nuisance parameter for correlations

        if 'pdgId' in dct.keys():
            self.pdg_id = dct['pdgId']
        if 'description' in dct.keys():
            self.text = dct['description']

        for attr in self.__dict__:
            if attr in dct.keys():
                self.__setattr__(attr, dct[attr])

        self.filename = filename  # name of json file that contains the parameter definition
        if self.name is None:
            self.name = filename.rsplit('.', 1)[0]

        if self.latex is None:
            self.latex = self.construct_latex(self.name)

        self.pdg_number = None # ordinal number used by PDG, obtained from PDG json
        self.pdg_value = None  # central value of PDG average, obtained from PDG json
        self.pdg_error = None  # error of PDG average, obtained from PDG json
        self.pdg_pubs = set()  # set of inspire IDs of publications that are included in the PDG average, obtained from PDGIdentifiers-references.json

        self.type = 'fit'      # type of parameter, one of 'fit', 'external', 'nuisance'
        self.measurements = [] # list of measurements that are sensitive to the parameter
        self.limits = []       # list of limits on the parameter
        self.pubs = set()      # set of inspire IDs of publications with measurements of the parameter
        self.sumwx = 0         # sum of weighted values, used to determine initial value of fit parameter
        self.sumw = 0          # sum of weights, used to determine initial value and uncertainty of fit parameter

        self.index = -1        # index of fit parameter

        self.average_value = None       # central value of fitted average
        self.average_error = None       # uncertainty of fitted average
        self.average_correlations = {}  # dictionary of parameter names to correlation values
        self.average_chi2 = 0           # chi2 of fit
        self.average_ndf = 0            # number of degrees of freedom of fit
        self.average_p = -1             # p-value of fitted average
        self.average_inputs = {}        # information about contributions of measurements to the average


    @staticmethod
    def construct_latex(name):
        """Construct the latex code from the parameter name."""

        if '..' in name:
            numerator, denominator = name.split('..', 1)
            numerator = Parameter.construct_latex(numerator)
            denominator = Parameter.construct_latex(denominator)
            if numerator is None or denominator is None:
                return None
            return f'\\dfrac{{{numerator}}}{{{denominator}}}'

        elif '.' in name:
            result = []
            for term in name.split('.'):
                latex = Parameter.construct_latex(term)
                if latex is None:
                    return None
                result.append(latex)
            return ' \\times '.join(result)

        elements = name.split('_')
        result = ''

        if len(elements) == 0:
            return None
        type = elements.pop(0)
        types = {
            'BR': '{\\cal B}',
            'ACP': 'A_\\mathrm{CP}',
            'Azero': 'A_0',
            'Aperp': 'A_\\perp',
            'Apara': 'A_\\parallel',
            'Deltaperp': '\\delta_\\perp',
            'Deltapara': '\\delta_\\parallel',
            'fL': "f_L",
            'fzero': 'f_0',
            'fperp': 'f_\\perp',
            'fpara': 'f_\\parallel',
            'AA': 'AA',
            'fc': 'f_c',
            'fLambdab0': 'f_{\\Lambda_b^0}',
            'fOmegab-': 'f_{\\Omega_b^-}',
            'fXib0Lambdab0': '\\dfrac{f_{\\Xi_b^0}}{f_{\\Lambda_b^0}}',
            'fXib-Lambdab0': '\\dfrac{f_{\\Xi_b^-}}{f_{\\Lambda_b^0}}',
            'alpha': '\\alpha',  # parity-violation asymmetry parameter
            'P': 'P',  # transverse polarization
            'IA': '\\Delta_{0-}'
        }
        if type not in types.keys():
            return None
        result = types[type]
        if len(elements) == 0:
            return result

        result += '('
        mother = elements.pop(0)
        if mother not in particle_indices.keys():
            return None
        result += particles[particle_indices[mother]][1] + ' \\to'

        for daughter in elements:
            if daughter not in particle_indices.keys():
                return None
            result += ' ' + particles[particle_indices[daughter]][1]

        result += ')'
        return result


    def check_name(self, convention=True):
        """Check if the name follows the conventions"""

        if allowed_characters.fullmatch(self.name) is None:
            print(f'WARNING: forbidden character(s) in parameter name {self.name}')

        if convention:
            name = re.sub('__.+?__', '_', self.name)
            name = re.sub('__.+', '', name)
            if self.construct_latex(name) is None:
                print(f'WARNING: the parameter name {self.name} does not follow the naming conventions')


    def create_nll(self, parameters, used_parameters, warn_unused=True):
        """Create the negative log likelihood function either from the given value or the nll string."""

        index = parameters[self.name].index

        if self.value is not None:
            if parameters[self.name].index < 0:
                if warn_unused:
                    print(f'WARNING: External parameter {self.name} is not used in any measurement.')
                self.nll = None
                return

            data = self.value

            # check for unit
            unit = 1
            match = re.search('(e|E)-?[0-9]+$', data)
            if match is not None:
                unit = eval('1' + match.group())
                data = data[:-len(match.group())]

            # parse central value and error
            value, data, precision = parse_number(data, unit)
            error, data, precision = parse_number(data, unit)

            self.average_value = value
            self.average_error = error
            self.sumw = error.weight()
            self.sumwx = self.sumw * value

            if error.symmetric():
                self.nll = f'nll_sym_gauss(par[{index}], {value}, {error.pos})'
            else:
                self.nll = f'nll_asym_gauss(par[{index}], {value}, {error.pos}, {abs(error.neg)})'

        elif self.nll is not None:
            if parameters[self.name].index < 0:
                if warn_unused:
                    print(f'WARNING: Nuisance/external parameter {self.name} is not used in any measurement.')
                self.nll = None
                return

            self.nll, additional_parameters = parse_formula(self.nll, parameters, used_parameters)


    def get_unit_digits(self):
        """Determine the exponent of the unit and the number of digits of the average"""

        if self.average_value == 0:
            if self.average_error is None:
                unit_exp = 0
            else:
                unit_exp = math.floor(math.log10(max(self.average_error.pos, -self.average_error.neg)))
        else:
            unit_exp = math.floor(math.log10(abs(self.average_value)))
        if abs(unit_exp) == 1:
            unit_exp = 0

        if self.average_error is None:
            value = self.average_value
        else:
            value = max(self.average_error.pos, -self.average_error.neg)
        digits = unit_exp - math.floor(math.log10(value / 10.))

        return (unit_exp, digits)


    def get_result_dict(self):
        """Return a dictionary with the fitted result for storage in a json file"""

        result = {
            'value': self.average_value,
            'error': errorToList(self.average_error),
            'correlations': self.average_correlations,
            'chi2': self.average_chi2,
            'ndf': self.average_ndf,
            'p': self.average_p,
            'inputs': self.average_inputs
        }
        return result


    def set_result_dict(self, result):
        """Take the fitted result from the given dictionary"""

        self.average_value = result.get('value', None)
        self.average_error = toError(result.get('error', None))
        self.average_correlations = result.get('correlations', {})
        self.average_chi2 = result.get('chi2', 0)
        self.average_ndf = result.get('ndf', 0)
        self.average_p = result.get('p', -1)
        self.average_inputs = result.get('inputs', {})


    def pdg_link(self):
        """Link to PDG live entry"""

        if self.pdg_id is None:
            return None
        elif 'Desig' in self.pdg_id:
            desig = self.pdg_id.split('=')[1]
            mode = self.pdg_id.split(':')[0]
            return f'https://pdglive.lbl.gov/BranchingRatio.action?desig={desig}&parCode={mode}'
        else:
            return f'https://pdglive.lbl.gov/DataBlock.action?node={self.pdg_id}'


    def pdg_diff(self):
        """Difference with respect to PDG value in standard deviations"""

        if self.average_value is None or self.pdg_value is None or self.average_error is None:
            return None
        diff = self.average_value - self.pdg_value
        error = -self.average_error.neg if diff > 0 else self.average_error.pos
        if error != 0:
            diff /= error
        else:
            diff = math.inf
        return diff


    def pdg_error_diff(self):
        """Factor (with largest relative deviation) between average and PDG uncertainties"""

        if self.average_error is None or self.pdg_error is None:
            return None
        diff_pos = math.inf if self.pdg_error.pos == 0 else self.average_error.pos / self.pdg_error.pos
        diff_neg = math.inf if self.pdg_error.neg == 0 else self.average_error.neg / self.pdg_error.neg
        diff_min = min(diff_pos, diff_neg)
        diff_max = max(diff_pos, diff_neg)
        if diff_min * diff_max > 1:
            return diff_max
        else:
            return diff_min
