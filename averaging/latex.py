#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import math
from .error import Error, toError


def latex_error(unit, digits, error):
    """Return latex code for an uncertainty."""

    if not isinstance(error, Error):
        error = toError(error)
    if error.symmetric() or (round(error.pos/unit, digits) == -round(error.neg/unit, digits)):
        return f' \\pm{error.pos/unit:.{digits}f}'
    else:
        return f'\,^{{+{error.pos/unit:.{digits}f}}}_{{{error.neg/unit:.{digits}f}}}'


def latex_result(unit, digits, value, stat_error, syst_error=None):
    """Return latex code for a measurement/average result in the given unit with "digits" digits after the dot."""

    digits = max(0, digits)
    if stat_error is None:
        return f'< {value/unit:.{digits}f}'
    result = f'{(value/unit):.{digits}f}'
    result += latex_error(unit, digits, stat_error)
    if syst_error is not None:
        result += latex_error(unit, digits, syst_error)
    return result


def latex_average(parameter):
    """Return latex code for average of a parameter."""

    unit_exp, digits = parameter.get_unit_digits()
    unit = 10 ** unit_exp
    result = latex_result(unit, digits, parameter.average_value, parameter.average_error)
    if unit_exp != 0:
        result += f' \\times 10^{{{unit_exp}}}'
    return result


class LatexOutput:
    """Class to generate latex code of the averaging results"""

    def __init__(self, config = None):
        """Create an output directory for the latex code."""

        if config is None:
            config = {}
        dir = config.get('latex_dir', 'latex')
        if not os.path.exists(dir):
            try:
                os.mkdir(dir)
            except OSError:
                print(f'ERROR: could not create directory {dir}')
                return
        self.dir = dir
        self.global_footnotes = config.get('latex_global_footnotes', False)
        self.pdg_always = config.get('latex_pdg_always', False)
        self.show_indirect = config.get('latex_show_indirect', False)
        self.warn_unknown = config.get('warn_unknown', False) or config.get('warn_all', False)
        self.warn_output = config.get('warn_output', False) or config.get('warn_all', False)
        self.output = None


    def write_header(self):
        """Write a latex header with own definitions to the main output file."""

        self.output.write(r"""\documentclass{article}
\usepackage[hyperfootnotes=false]{hyperref}
\usepackage{float}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage[nointegrals]{wasysym}
\usepackage[multiple]{footmisc}
\usepackage{threeparttable}
\usepackage[math]{cellspace}
\cellspacetoplimit 2pt
\cellspacebottomlimit 2pt

\newlength{\hflavrowwidth}
\newcommand{\hflavrowdefshort}[4]{\def#1{#2 & #3 & #4}}
\newcommand{\hflavrowdeflong}[4]{\def#1{\multicolumn{3}{|Sl|}{#2}\\ & #3 & #4}}
\newcommand{\hflavrowdef}[4]{\settowidth{\hflavrowwidth}{\begin{tabular}{| l l l |} \\ #2 & #3 & #4 \\ \end{tabular}}\ifnum \hflavrowwidth>\textwidth \hflavrowdeflong{#1}{#2}{#3}{#4} \else \hflavrowdefshort{#1}{#2}{#3}{#4} \fi}

\begin{document}

""")


    def write_footer(self, label):
        """Write a latex footer with bibliography definition to the main output file."""

        self.output.write(r"""
\bibliographystyle{plain}
\raggedright
\bibliography{""" + label + """}

\end{document}
""")


    def write_section(self, levels, caption):
        """Write a section of given level to the main output file."""

        latex_levels = ['section', 'subsection', 'subsubsection', 'paragraph']
        label = '_'.join(levels)
        self.output.write(f'\n\\{latex_levels[len(levels)-1]}{{{caption}}}\n\\label{{sec:{label}}}\n')


    def latex_comment(self, comments):
        """Return latex code for footnotes with the given comments."""

        if comments is None:
            return ''

        if not isinstance(comments, list):
            comments = [comments]

        result = []
        for comment in comments:
            if comment in self.averaging.comments.keys():
                comment = self.averaging.comments[comment]
            if comment is False:
                continue

            if '^' in comment and '$' not in comment:
                comment = '$' + comment + '$'
            if '_' in comment and '$' not in comment:
                comment = comment.replace('_', r'\_')
            if '%' in comment and r'\%' not in comment:
                comment = comment.replace('%', r'\%')

            if comment not in self.footnotes:
                self.footnotes.append(comment)
            index = self.footnotes.index(comment)
            result.append(index+1)

        footnotes = ','.join([str(index) for index in result])
        return f'\\tnote{{{footnotes}}}\\hphantom{{\\textsuperscript{{{footnotes}}}}}'


    def latex_ref(self, publication):
        """Return latex code for citing the given publication."""

        if publication.bibtex:
            if publication.bibtex_id not in self.bibtex.keys():
                self.bibtex[publication.bibtex_id] = publication.bibtex
            return f'\\cite{{{publication.bibtex_id}}}'
        return ''


    def write_parameter_def(self, table, name, rowdef, unit=None, format=None, options=None, features=None):
        """Write a definition of rowdef for a table row with parameter, measurements (and PDG average if existent), and our average to the given table file."""

        if self.warn_output and name in self.parameters:
            print(f'WARNING: parameter {name} appears multiple times in latex output')
        self.parameters.append(name)

        parameter = self.averaging.parameters[name]
        if parameter.average_value is None:
            return False

        # unit and precision
        unit_str = ''
        unit_exp, digits = parameter.get_unit_digits()
        if unit is None:
            unit = 10 ** unit_exp
            if unit_exp != 0:
                unit_str = f' [$10^{{{unit_exp}}}$]'
        else:
            digits += math.floor(math.log10(unit)) - unit_exp;
            unit_exp = math.floor(math.log10(unit))
        rowstyle = ''
        if format is not None:
            if format[0] == 's':
                rowstyle = 'short'
                format = format[1:]
            elif format[0] == 'l':
                rowstyle = 'long'
                format = format[1:]
            if format.startswith('.'):
                digits = int(format[1:])
            elif len(format) > 0:
                digits = max(math.floor(math.log10(unit)) - int(format), 0)


        # parameter with unit
        latex_parameter = f'${parameter.latex}$'
        latex_parameter += self.latex_comment(parameter.comment)
        latex_parameter += unit_str

        # measurements
        latex_measurements = '\\begin{tabular}{l} '
        indirect_measurements = {}
        for measurement_hash, input in parameter.average_inputs.items():
            if measurement_hash in self.averaging.parameters.keys():
                continue
            measurement = self.averaging.measurements[measurement_hash]
            if input['value'] is None and not self.show_indirect:
                if measurement.publication.experiment not in indirect_measurements.keys():
                    indirect_measurements[measurement.publication.experiment] = []
                indirect_measurements[measurement.publication.experiment].append([measurement, input])
                continue
            latex_measurements += '{'
            color = measurement.get_color(parameter)
            if color is not None and (options is None or 'nopdg' not in options or color != 'red'):
                latex_measurements += f'\\color{{{color}}}'
                if features is not None:
                    features.add(color)
            latex_measurements += measurement.publication.experiment
            latex_measurements += ' ' + self.latex_ref(measurement.publication)
            latex_measurements += self.latex_comment(measurement.publication.comment)
            if input['value'] is not None:
                latex_measurements += f' ${latex_result(unit, unit_exp - math.floor(math.log10(input["precision"])), input["value"], toError(input.get("stat_error", None)), toError(input.get("syst_error", None)))}$ '
                if features is not None and input.get("stat_error", None) is None:
                    features.add('limit')
            else:
                latex_measurements += f' \\tiny ${input["latex"]}$ '
            comments = []
            if measurement.comment is not None:
                if isinstance(measurement.comment, list):
                    comments += measurement.comment
                else:
                    comments.append(measurement.comment)
            if len(input.get('corrections', [])) > 0 and input['value'] is not None:
                comments.append('Using ' + ', '.join([f'${self.averaging.parameters[name].latex}$.' for name in input['corrections']]))
            latex_measurements += self.latex_comment(comments)
            latex_measurements += '} \\\\ '
        for experiment in sorted(indirect_measurements.keys()):
            latex_measurements += experiment + ' '
            count = 0
            for measurement, input in indirect_measurements[experiment]:
                if count > 0:
                    latex_measurements += ', '
                count += 1
                if count == 5:
                    latex_measurements += ' \\\\ '
                latex_measurements += '{'
                color = measurement.get_color(parameter)
                if color is not None and (options is None or 'nopdg' not in options or color != 'red'):
                    latex_measurements += f'\\color{{{color}}}'
                latex_measurements += self.latex_ref(measurement.publication)
                comments = []
                if measurement.comment is not None:
                    if isinstance(measurement.comment, list):
                        comments += measurement.comment
                    else:
                        comments.append(measurement.comment)
                comments.append(f'Measurement of ${input["latex"]}$ used in our fit.')
                latex_measurements += self.latex_comment(comments)
                latex_measurements += '}'
            latex_measurements += ' \\\\ '
        latex_measurements += '\\end{tabular}'

        # average and PDG
        latex_average = '\\begin{tabular}{l} '
        average_result = latex_result(unit, digits, parameter.average_value, parameter.average_error)
        latex_average += f'${average_result}$'
        if parameter.average_error is not None and parameter.average_p is not None and parameter.average_p < 0.01:
            latex_average += ' \\\\[-5pt] {\\tiny p=' + f'{1000*parameter.average_p:.1f}' + '\\permil}'
            if features is not None:
                features.add('p-value')
        if not self.pdg_always:
            if options is None or 'nopdg' not in options:
                if parameter.pdg_value is None:
                    latex_average += f' \\\\ \color{{gray}} none'
                    if features is not None:
                        features.add('pdg')
                else:
                    pdg_result = latex_result(unit, digits, parameter.pdg_value, parameter.pdg_error)
                    if pdg_result != average_result:
                        latex_average += f' \\\\ \color{{gray}}${pdg_result}$'
                        if features is not None:
                            features.add('pdg')
        else:
            if parameter.pdg_value is not None and (options is None or 'nopdg' not in options):
                latex_average += f' \\\\ \color{{gray}}${latex_result(unit, digits, parameter.pdg_value, parameter.pdg_error)}$'
        latex_average += '\\\\ \\end{tabular}'

        # table row definition
        table.write(f'\\hflavrowdef{rowstyle}{{{rowdef}}}{{{latex_parameter}}}{{{latex_measurements}}}{{{latex_average}}}\n')
        return True


    def write_table(self, levels, caption, parameters, unit=None, options=None):
        """Write a table with the given parameters to the main output file."""

        label = '_'.join(levels)
        if label.endswith('_'):
            label = label[:-1]
        if not self.global_footnotes:
            self.footnotes = []
        nfootnotes = len(self.footnotes)
        features = set()
        resize = None
        footnoteprefix = None
        if options is not None:
            for option in options:
                suboptions = option.split(':')
                if suboptions[0] == 'resize':
                    resize = ''
                    if len(suboptions) > 1:
                        resize = suboptions[1]
                elif suboptions[0] == 'footnoteprefix' and len(suboptions) > 1:
                    footnoteprefix = suboptions[1]

        with open(os.path.join(self.dir, label + '.tex'), 'w') as table:
            rowdefs = []
            for name in parameters:
                format = None
                if ':' in name:
                    name, format = name.split(':')
                if name not in self.averaging.parameters.keys():
                    if self.warn_unknown:
                        print(f'WARNING: parameter {name} unknown')
                    continue
                rowdef = f'\\hflavrowdef{chr(97 + int(self.rowcount / (26*26)))}{chr(97 + (int(self.rowcount / 26) % 26))}{chr(97 + self.rowcount % 26)}'
                if self.write_parameter_def(table, name, rowdef, unit, format, options, features):
                    self.rowcount += 1
                    rowdefs.append(rowdef)
            table.write('\\begin{table}[H]\n\\begin{center}\n')
            if resize is None:
                table.write('%')
            table.write(f'\\resizebox{{{resize}\\textwidth}}{{!}}{{\n')
            table.write('\\begin{threeparttable}\n')

            for feature in features:
                add_caption = self.averaging.comments.get(f'caption_{feature}', None)
                if add_caption is not None:
                    caption += '. ' + add_caption
            table.write(f'\\caption{{{caption}.}}\n\\label{{tab:{label}}}\n')
            table.write('\\begin{tabular}{| Sl l l |}\n')
            unit_str = '' if unit is None or unit == 1 else f' [$10^{{{math.floor(math.log10(unit))}}}$]'
            average_str = ' $^\\mathrm{HFLAV}_\\mathrm{\\color{gray}PDG}$' if options is None or 'nopdg' not in options else ''
            table.write(rf"""\hline
\textbf{{Parameter{unit_str}}} & \begin{{tabular}}{{l}}\textbf{{Measurements}}\end{{tabular}} & \begin{{tabular}}{{l}}\textbf{{Average{average_str}}}\end{{tabular}} \\
\hline
\hline
""")
            for rowdef in rowdefs:
                table.write(f'{rowdef} \\\\\n\\hline\n')
            table.write('\\end{tabular}\n')
            if len(self.footnotes) > nfootnotes:
                table.write('\\begin{tablenotes}\n')
                if footnoteprefix is not None:
                    table.write(f'{footnoteprefix}\n')
                for index in range(nfootnotes, len(self.footnotes)):
                    table.write(f'\\item[{index+1}] {self.footnotes[index]}\n')
                table.write('\\end{tablenotes}\n')
            table.write('\\end{threeparttable}\n')
            if resize is None:
                table.write('%')
            table.write('}\n')
            table.write('\\end{center}\n\\end{table}\n')

        self.output.write(f'\\input{{{label}}}\n')


    def traverse(self, structure, levels, unit=None):
        """Traverse the given structure and write sections and tables to the main output file."""

        label = structure.get('label', None)
        levels.append(label)
        caption = structure.get('caption', None)
        if 'unit' in structure.keys():
            unit = eval(structure['unit'])
        if 'parameters' in structure.keys():
            parameters = self.averaging.filter(structure.get('parameters'))
            if parameters:
                self.write_table(levels, caption, parameters, unit, structure.get('options', None))
        else:
            self.write_section(levels, caption)
            for subsection in structure.get('subsections', []):
                self.traverse(subsection, levels + [], unit)


    def write_bib(self, label):
        """Write a bib file with the bibtex entries."""

        with open(os.path.join(self.dir, label + '.bib'), 'w') as bibfile:
            for bibtex in self.bibtex.values():
                bibfile.write(bibtex.replace('&gt;', '>'))


    def run(self, averaging, structure):
        """Produce latex code for the given averages using the given structure.

The structure is a dictionary with the follwing keys
- "label": an identifier for latex labels and file names,
- "caption": a title of the section or table,
- "parameters" (optional): a list of parameter names that will be put into a table
- "subsections" (optional): a list of dictionaries of structures of the next level
"""

        self.averaging = averaging
        self.bibtex = {}
        self.footnotes = []
        self.rowcount = 0
        self.parameters = []
        label = structure['label']
        with open(os.path.join(self.dir, label + '.tex'), 'w') as self.output:
            self.write_header()
            self.traverse(structure, [])
            self.write_footer(label)
        self.write_bib(label)
        if self.warn_output:
            for name in self.averaging.parameters.keys():
                parameter = self.averaging.parameters[name]
                if name not in self.parameters and parameter.type == 'fit' and parameter.average_value is not None:
                    print(f'WARNING: parameter {name} not included in latex output')
