#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from .measurement import Measurement
import re

class Publication:
    """Representation of a publication with measurements."""

    def __init__(self, dct):
        """Initialize the publication object from a dictionary."""

        self.inspire = None      # inspire identifier
        self.arxiv = None        # arXiv identified
        self.journal = None      # journal information: title, volume, id, year
        self.doi = None          # digital object identifier
        self.altref = None       # array of reference name and link that will be used if no journal reference or arXiv preprint is available
        self.experiment = None   # name of collaboration
        self.comment = None      # a literal comment or an identifier of a comment or an array of those that will be displayed as footnote(s)
        self.superseded = False  # a flag that indicates if the result is superseded
        self.bibtex = None       # a string containing the bibtex entry
        self.measurements = []   # array of measurements

        for attr in self.__dict__:
            if attr in dct.keys():
                self.__setattr__(attr, dct[attr])

        if self.bibtex is not None:
            self.bibtex_id = re.findall('@\w+\{(\S+),\n[\S\s]*', self.bibtex)[0]

        for entry in self.measurements:
            entry.publication = self


    def link_text(self):
        """link url and text to publication"""

        text = None
        link = None
        if self.inspire is not None:
            text = f'inspire id {self.inspire}'
            link = f'https://inspirehep.net/literature/{self.inspire}'
        if self.arxiv is not None:
            text = f'arXiv:{self.arxiv}'
            if link is None:
                link =  f'https://arxiv.org/abs/{self.arxiv}'
        if self.journal is not None:
            text = f'{self.journal["title"]} <b>{self.journal["volume"]}</b>,{self.journal["id"]} ({self.journal["year"]})'
        if text is None:
            text = self.altref[0]
        if link is None:
            link = self.altref[1]

        return (link, text)


    def link(self):
        """Html link to publication"""

        link, text = self.link_text()
        return f'<a href="{link}">{text}</a>'


    def color(self, parameter):
        """Color for preliminary results or results not included in the PDG average"""

        if self.journal is None:
            return 'blue'
        elif self.inspire not in parameter.pdg_pubs:
            return 'red'
        return None
