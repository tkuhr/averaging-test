#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import sys
from averaging.parser import allowed_characters, parse_formula
from averaging.parameter import Parameter


parser = argparse.ArgumentParser(description='HFLAV tool to rename a parameter')
parser.add_argument('-i', '--ignore', dest='ignore', action='store_true', help='rename even if the new parameter contains invalid characters')
parser.add_argument('-f', '--force', dest='force', action='store_true', help='rename even if the new parameter already exists')
parser.add_argument('from_name', metavar='FROM', help='old name of the parameter')
parser.add_argument('to_name', metavar='TO', help='new name of the parameter')
parser.add_argument('files', metavar='FILES', nargs='*', help='names of files in which the parameter should be renamed, default are all json files in and below the current directory')
args = parser.parse_args()

# check new parameter name for invalid characters
if allowed_characters.fullmatch(args.to_name) is None:
    if not args.ignore:
        print('ERROR: the new parameter name contains invalid characters')
        sys.exit(1)

# determine list of files
files = args.files
if len(files) == 0:
    for root, dirs, filenames in os.walk('.'):
        for filename in filenames:
            if filename.endswith('.json'):
                files.append(os.path.join(root, filename))

# check if new name is already used
parameters = {args.to_name: Parameter({}, 'par')}
used_parameters = []
filenames = []
for filename in files:
    with open(filename) as json_file:
        if 'par[0]' in parse_formula(json_file.read(), parameters, used_parameters)[0]:
            filenames.append(filename)
if len(filenames) > 0:
    if args.force:
        print(f'WARNING: {args.to_name} already used in {", ".join(filenames)}')
    else:
        print(f'ERROR: {args.to_name} already used in {", ".join(filenames)}')
        sys.exit(1)

# rename parameter
parameters = {args.from_name: Parameter({}, 'par')}
used_parameters = []
filenames = []
for filename in files:
    with open(filename) as json_file:
        content = parse_formula(json_file.read(), parameters, used_parameters)[0]
        if 'par[0]' in content:
            filenames.append(filename)
    if filename in filenames:
        with open(filename, 'w') as json_file:
            json_file.write(content.replace('par[0]', args.to_name))
if len(filenames) > 0:
    print(f'{args.from_name} renamed to {args.to_name} in {", ".join(filenames)}')
else:
    print(f'WARNING: {args.from_name} not found in any files')
